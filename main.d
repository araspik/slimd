module start;

import std.stdio;
import gcc.attribute;

pragma(constructor, 101) extern(C) void test() {
	stdout.write("Test?\n");
}

int main(string[] args) {

	char[50] str = void;
	stdout.write("Text (<50 chars): ");
	stdout.read(str[]);

	File file = File.open("test.txt", FileMode.wo | FileMode.create | FileMode.trunc);
	file.write(str[]);
	file.close();

	return 0;
}
