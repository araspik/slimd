/**
	* Forms the symbols available to all D programs. Includes Object, which is
	* the root of the class object hierarchy.	This module is implicitly
	* imported.
	*
	* Copyright: Copyright Digital Mars 2000 - 2011.
	* License:		$(WEB www.boost.org/LICENSE_1_0.txt, Boost License 1.0).
	* Authors:		Walter Bright, Sean Kelly
	*/

module object;

alias size_t = typeof(void.sizeof);
alias ptrdiff_t = typeof(cast(void*)0 - cast(void*)0);
alias  string = immutable( char)[];
alias wstring = immutable(wchar)[];
alias dstring = immutable(dchar)[];

// Backwards compatability
deprecated {
	alias sizediff_t = ptrdiff_t;
	alias hash_t = size_t;
	alias equals_t = bool;
}

extern(C) @trusted nothrow {
	import gcc.attribute;
	/// Provides array casting.
	void[] _d_arraycast(size_t tsz, size_t fsz, void[] a) pure {
		assert ((a.length * fsz) % tsz == 0);
		return a.ptr[0 .. a.length * fsz / tsz];
	}

	/// Provides array copying.
	pragma(inline, false) @attribute("noinline") void[] _d_arraycopy(in size_t size, const void[] from, void[] to)
	 in {
		assert (from.length == to.length);
	 } do {
		import core.memops: memcpy;
		assert ((from.ptr < to.ptr && (from.ptr + size * to.length) < to.ptr)
				|| (to.ptr + size * to.length) < from.ptr);
		memcpy(to.ptr, from.ptr, to.length * size);
		return to;
	}

}

/// Common parent for all classes.
class Object {

 @safe @nogc nothrow:

	/// Convert to a human-readable string.
	string toString() {return "";}

	/// Compute hash.
	size_t toHash() {return 0;}

	/// Compares with another Object.
	int opCmp(Object o) {return 0;}

	/// Tests for equality.
	bool opEquals(Object o) {return false;}

	/// Monitor.
	interface Monitor {
		void lock();
		void unlock();
	}

	/// Creates class given name.
	static Object factory(string classname) {return null;}
}

bool opEquals(Object lhs, Object rhs) {return false;}
auto opEquals(const Object lhs, const Object rhs) {return false;}

/// Interface info.
struct Interface {
	TypeInfo_Class	 classinfo;
	void*[]		vtbl;
	size_t		offset;
}

struct OffsetTypeInfo {
	size_t	 offset;	/// Offset of member from start of object
	TypeInfo ti;		/// TypeInfo for this member
}

class TypeInfo {
	size_t getHash(in void* p) @trusted nothrow const { return 0; }
	bool equals(in void* p1, in void* p2) const { return false; }
	int compare(in void* p1, in void* p2) const { return 0; }
	@property size_t tsize() nothrow pure const @safe @nogc { return 0; }
	void swap(void* p1, void* p2) const {}
	@property inout(TypeInfo) next() nothrow pure inout @nogc { return null; }
	abstract const(void)[] initializer() nothrow pure const @safe @nogc;
	@property uint flags() nothrow pure const @safe @nogc { return 0; }
	const(OffsetTypeInfo)[] offTi() const { return null; }
	void destroy(void* p) const {}
	void postblit(void* p) const {}
	@property size_t talign() nothrow pure const @safe @nogc { return tsize; }
	int argTypes(out TypeInfo arg1, out TypeInfo arg2) @safe nothrow {return 0;}
	@property immutable(void)* rtInfo() nothrow pure const @safe @nogc { return null; }
}

class TypeInfo_Enum : TypeInfo {
	TypeInfo base;
	string	 name;
	void[]	 m_init;
}

class TypeInfo_Pointer : TypeInfo {
	TypeInfo m_next;
}

class TypeInfo_Array : TypeInfo {
}

class TypeInfo_StaticArray : TypeInfo {
	TypeInfo value;
	size_t	 len;
}

class TypeInfo_AssociativeArray : TypeInfo {
	TypeInfo value;
	TypeInfo key;
}

class TypeInfo_Vector : TypeInfo {
	TypeInfo base;
}

class TypeInfo_Function : TypeInfo {
	TypeInfo next;
	string deco;
}

class TypeInfo_Delegate : TypeInfo {
	TypeInfo next;
	string deco;
}

class TypeInfo_Class : TypeInfo {
	@property auto info() @safe nothrow pure const { return this; }
	@property auto typeinfo() @safe nothrow pure const { return this; }
	byte[]		m_init;
	string		name;				/// class name
	void*[]		vtbl;				/// virtual function pointer table
	Interface[] interfaces;		/// interfaces this class implements
	TypeInfo_Class	 base;			 /// base class
	void*			destructor;
	void function(Object) classInvariant;
	enum ClassFlags : uint {
		isCOMclass = 0x1,
		noPointers = 0x2,
		hasOffTi = 0x4,
		hasCtor = 0x8,
		hasGetMembers = 0x10,
		hasTypeInfo = 0x20,
		isAbstract = 0x40,
		isCPPclass = 0x80,
		hasDtor = 0x100,
	}
	ClassFlags m_flags;
	void*			deallocator;
	OffsetTypeInfo[] m_offTi;
	void function(Object) defaultConstructor;		// default Constructor

	immutable(void)* m_RTInfo;		// data for precise GC
	static const(TypeInfo_Class) find(in char[] classname) {
		return null;
	}

	/// Create instance of Object represented by 'this'.
	Object create() const {
		return null;
	}
}

alias ClassInfo = TypeInfo_Class;

class TypeInfo_Interface : TypeInfo {
	TypeInfo_Class info;
}

class TypeInfo_Struct : TypeInfo {
	string name;
	void[] m_init;		// initializer; m_init.ptr == null if 0 initialize

	@safe pure nothrow {
	size_t	 function(in void*)				xtoHash;
	bool	 function(in void*, in void*) xopEquals;
	int		 function(in void*, in void*) xopCmp;
	string	 function(in void*)				xtoString;

	enum StructFlags : uint {
		hasPointers = 0x1,
		isDynamicType = 0x2, // built at runtime, needs type info in xdtor
	}
	StructFlags m_flags;
	}
	union {
		void function(void*)				xdtor;
		void function(void*, const TypeInfo_Struct ti) xdtorti;
	}
	void function(void*)					xpostblit;

	uint m_align;

	immutable(void)* m_RTInfo;				// data for precise GC
}

class TypeInfo_Tuple : TypeInfo {
	TypeInfo[] elements;
}

class TypeInfo_Const : TypeInfo {
	TypeInfo base;
}

class TypeInfo_Invariant : TypeInfo_Const {
}

class TypeInfo_Shared : TypeInfo_Const {
}

class TypeInfo_Inout : TypeInfo_Const {
}


///////////////////////////////////////////////////////////////////////////////
// ModuleInfo
///////////////////////////////////////////////////////////////////////////////


enum {
	MIctorstart  = 0x1,		// we've started constructing it
	MIctordone	 = 0x2,		// finished construction
	MIstandalone = 0x4,		// module ctor does not depend on other module
						// ctors being done first
	MItlsctor  = 8,
	MItlsdtor  = 0x10,
	MIctor		 = 0x20,
	MIdtor		 = 0x40,
	MIxgetMembers = 0x80,
	MIictor		 = 0x100,
	MIunitTest	 = 0x200,
	MIimportedModules = 0x400,
	MIlocalClasses = 0x800,
	MIname		 = 0x1000,
}


struct ModuleInfo {
	uint _flags;
	uint _index; // index into _moduleinfo_array[]

	version (all) {
		deprecated("ModuleInfo cannot be copy-assigned because it is a variable-sized struct.")
		void opAssign(in ModuleInfo m) { _flags = m._flags; _index = m._index; }
	}
	else {
		@disable this();
		@disable this(this) const;
	}

const:
	private void* addrOf(int flag) nothrow pure @nogc {return cast(void*)null;}
	@property uint index() nothrow pure @nogc { return 0; }
	@property uint flags() nothrow pure @nogc { return 0; }
	@property void function() tlsctor() nothrow pure @nogc {return null;}
	@property void function() tlsdtor() nothrow pure @nogc {return null;}
	@property void* xgetMembers() nothrow pure @nogc {return null;}
	@property void function() ctor() nothrow pure @nogc {return null;}
	@property void function() dtor() nothrow pure @nogc {return null;}
	@property void function() ictor() nothrow pure @nogc {return null;}
	@property void function() unitTest() nothrow pure @nogc {return null;}
	@property immutable(ModuleInfo*)[] importedModules() nothrow pure @nogc {
		return null;}
	@property TypeInfo_Class[] localClasses() nothrow pure @nogc {return null;}
	@property string name() nothrow pure @nogc {return null;}
	static int opApply(scope int delegate(ModuleInfo*) dg) {return 0;}
}

class Throwable : Object {
	interface TraceInfo {
		int opApply(scope int delegate(ref const(char[]))) const;
		int opApply(scope int delegate(ref size_t, ref const(char[]))) const;
		string toString() const;
	}
	string		msg;	/// A message describing the error.
	string		file;
	size_t		line;
	TraceInfo		info;
	Throwable		next;

	@nogc @safe pure nothrow this(string msg, Throwable next = null) {
		this.msg = msg;
		this.next = next;
		//this.info = _d_traceContext();
	}

	@nogc @safe pure nothrow this(string msg, string file, size_t line, Throwable next = null) {
		this(msg, next);
		this.file = file;
		this.line = line;
		//this.info = _d_traceContext();
	}

	void toString(scope void delegate(in char[]) sink) const {}
}

class Exception : Throwable {
	@nogc @safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null) {
		super(msg, file, line, next);
	}
	@nogc @safe pure nothrow this(string msg, Throwable next, string file = __FILE__, size_t line = __LINE__) {
		super(msg, file, line, next);
	}
}

class Error : Throwable {
	@nogc @safe pure nothrow this(string msg, Throwable next = null) {
		super(msg, next);
		bypassedException = null;
	}
	@nogc @safe pure nothrow this(string msg, string file, size_t line, Throwable next = null) {
		super(msg, file, line, next);
		bypassedException = null;
	}
	Throwable		bypassedException;
}

/* Used in Exception Handling LSDA tables to 'wrap' C++ type info
	* so it can be distinguished from D TypeInfo
	*/
class __cpp_type_info_ptr {
	void* ptr;			// opaque pointer to C++ RTTI type info
}

// RIP: Associative array code ---> | <---

private void _destructRecurse(S)(ref S s)
	if (is(S == struct)) {
	static if (__traits(hasMember, S, "__xdtor") &&
				 // Bugzilla 14746: Check that it's the exact member of S.
				 __traits(isSame, S, __traits(parent, s.__xdtor)))
		s.__xdtor();
}

private void _destructRecurse(E, size_t n)(ref E[n] arr) {
	import core.internal.traits : hasElaborateDestructor;

	static if (hasElaborateDestructor!E) {
		foreach_reverse (ref elem; arr)
			_destructRecurse(elem);
	}
}

// Public and explicitly undocumented
void _postblitRecurse(S)(ref S s)
	if (is(S == struct)) {
	static if (__traits(hasMember, S, "__xpostblit") &&
				 // Bugzilla 14746: Check that it's the exact member of S.
				 __traits(isSame, S, __traits(parent, s.__xpostblit)))
		s.__xpostblit();
}

// Ditto
void _postblitRecurse(E, size_t n)(ref E[n] arr) {}

/// ditto
void destroy(T)(ref T obj) if (is(T == struct)) {
	_destructRecurse(obj);
	() @trusted {
		auto buf = (cast(ubyte*) &obj)[0 .. T.sizeof];
		auto init = cast(ubyte[])typeid(T).initializer();
		if (init.ptr is null) // null ptr means initialize to 0s
			buf[] = 0;
		else
			buf[] = init[];
	} ();
}

/// ditto
void destroy(T : U[n], U, size_t n)(ref T obj) if (!is(T == struct)) {
	foreach_reverse (ref e; obj[])
		destroy(e);
}

/// ditto
void destroy(T)(ref T obj)
	if (!is(T == struct) && !is(T == interface) && !is(T == class) && !_isStaticArray!T) {
	obj = T.init;
}

template _isStaticArray(T : U[N], U, size_t N) {
	enum bool _isStaticArray = true;
}

template _isStaticArray(T) {
	enum bool _isStaticArray = false;
}

/***************************************
	* Helper function used to see if two containers of different
	* types have the same contents in the same sequence.
	*/

bool _ArrayEq(T1, T2)(T1[] a1, T2[] a2) {
	if (a1.length != a2.length)
		return false;

	// This is function is used as a compiler intrinsic and explicitly written
	// in a lowered flavor to use as few CTFE instructions as possible.
	size_t idx = 0;
	immutable length = a1.length;

	for(;idx < length;++idx) {
		if (a1[idx] != a2[idx])
			return false;
	}
	return true;
}

bool _xopEquals(in void*, in void*) {return false;}
bool _xopCmp(in void*, in void*) {return false;}

void __ctfeWrite(const string s) @nogc @safe pure nothrow {}

enum RTInfo(T) = null;

// lhs == rhs lowers to __equals(lhs, rhs) for dynamic arrays
bool __equals(T1, T2)(T1[] lhs, T2[] rhs) {
	import std.ctfe : Unqual;
	alias U1 = Unqual!T1;
	alias U2 = Unqual!T2;

	static @trusted ref R at(R)(R[] r, size_t i) { return r.ptr[i]; }
	static @trusted R trustedCast(R, S)(S[] r) { return cast(R) r; }

	if (lhs.length != rhs.length)
		return false;

	if (lhs.length == 0 && rhs.length == 0)
		return true;

	static if (is(U1 == void) && is(U2 == void)) {
		return __equals(trustedCast!(ubyte[])(lhs), trustedCast!(ubyte[])(rhs));
	}
	else static if (is(U1 == void)) {
		return __equals(trustedCast!(ubyte[])(lhs), rhs);
	}
	else static if (is(U2 == void)) {
		return __equals(lhs, trustedCast!(ubyte[])(rhs));
	}
	else static if (!is(U1 == U2)) {
		// This should replace src/object.d _ArrayEq which
		// compares arrays of different types such as long & int,
		// char & wchar.
		// Compiler lowers to __ArrayEq in dmd/src/opover.d
		foreach (const u; 0 .. lhs.length) {
			if (at(lhs, u) != at(rhs, u))
				return false;
		}
		return true;
	}
	else static if (__traits(isIntegral, U1)) {

		if (!__ctfe) {
			import core.memops : memcmp;
			return () @trusted { return memcmp(cast(void*)lhs.ptr, cast(void*)rhs.ptr, lhs.length	* U1.sizeof) == 0; }();
		}
		else {
			foreach (const u; 0 .. lhs.length) {
				if (at(lhs, u) != at(rhs, u))
					return false;
			}
			return true;
		}
	}
	else {
		foreach (const u; 0 .. lhs.length) {
			static if (__traits(compiles, __equals(at(lhs, u), at(rhs, u)))) {
				if (!__equals(at(lhs, u), at(rhs, u)))
					return false;
			}
			else static if (__traits(isFloating, U1)) {
				if (at(lhs, u) != at(rhs, u))
					return false;
			}
			else static if (is(U1 : Object) && is(U2 : Object)) {
				if (!(cast(Object)at(lhs, u) is cast(Object)at(rhs, u)
					|| at(lhs, u) && (cast(Object)at(lhs, u)).opEquals(cast(Object)at(rhs, u))))
					return false;
			}
			else static if (__traits(hasMember, U1, "opEquals")) {
				if (!at(lhs, u).opEquals(at(rhs, u)))
					return false;
			}
			else static if (is(U1 == delegate)) {
				if (at(lhs, u) != at(rhs, u))
					return false;
			}
			else static if (is(U1 == U11*, U11)) {
				if (at(lhs, u) != at(rhs, u))
					return false;
			}
			else {
				if (at(lhs, u).tupleof != at(rhs, u).tupleof)
					return false;
			}
		}

		return true;
	}
}

int __cmp(T)(const T[] lhs, const T[] rhs) @trusted
if (__traits(isScalar, T)) {
	// Compute U as the implementation type for T
	static if (is(T == ubyte) || is(T == void) || is(T == bool))
		alias U = char;
	else static if (is(T == wchar))
		alias U = ushort;
	else static if (is(T == dchar))
		alias U = uint;
	else static if (is(T == ifloat))
		alias U = float;
	else static if (is(T == idouble))
		alias U = double;
	else static if (is(T == ireal))
		alias U = real;
	else
		alias U = T;

	static if (is(U == char)) {
		import core.utils: dstrcmp;
		return dstrcmp(cast(char[]) lhs, cast(char[]) rhs);
	}
	else static if (!is(U == T)) {
		// Reuse another implementation
		return __cmp(cast(U[]) lhs, cast(U[]) rhs);
	}
	else {
		immutable len = lhs.length <= rhs.length ? lhs.length : rhs.length;
		foreach (const u; 0 .. len) {
			static if (__traits(isFloating, T)) {
				immutable a = lhs.ptr[u], b = rhs.ptr[u];
				static if (is(T == cfloat) || is(T == cdouble)
					|| is(T == creal)) {
					// Use rt.cmath2._Ccmp instead ?
					auto r = (a.re > b.re) - (a.re < b.re);
					if (!r) r = (a.im > b.im) - (a.im < b.im);
				}
				else {
					const r = (a > b) - (a < b);
				}
				if (r) return r;
			}
			else if (lhs.ptr[u] != rhs.ptr[u])
				return lhs.ptr[u] < rhs.ptr[u] ? -1 : 1;
		}
		return lhs.length < rhs.length ? -1 : (lhs.length > rhs.length);
	}
}

// This function is called by the compiler when dealing with array
// comparisons in the semantic analysis phase of CmpExp. The ordering
// comparison is lowered to a call to this template.
int __cmp(T1, T2)(T1[] s1, T2[] s2)
if (!__traits(isScalar, T1)) {
	import std.ctfe : Unqual;
	alias U1 = Unqual!T1;
	alias U2 = Unqual!T2;
	static assert(is(U1 == U2), "Internal error.");

	static if (is(U1 == void))
		static @trusted ref inout(ubyte) at(inout(void)[] r, size_t i) { return (cast(inout(ubyte)*) r.ptr)[i]; }
	else
		static @trusted ref R at(R)(R[] r, size_t i) { return r.ptr[i]; }

	// All unsigned byte-wide types = > dstrcmp
	immutable len = s1.length <= s2.length ? s1.length : s2.length;

	foreach (const u; 0 .. len) {
		static if (__traits(compiles, __cmp(at(s1, u), at(s2, u)))) {
			auto c = __cmp(at(s1, u), at(s2, u));
			if (c != 0)
				return c;
		}
		else static if (__traits(compiles, at(s1, u).opCmp(at(s2, u)))) {
			auto c = at(s1, u).opCmp(at(s2, u));
			if (c != 0)
				return c;
		}
		else static if (__traits(compiles, at(s1, u) < at(s2, u))) {
			if (at(s1, u) != at(s2, u))
				return at(s1, u) < at(s2, u) ? -1 : 1;
		}
		else {
			// TODO: fix this legacy bad behavior, see
			// https://issues.dlang.org/show_bug.cgi?id=17244
			import core.stdc.string : memcmp;
			return (() @trusted => memcmp(&at(s1, u), &at(s2, u), U1.sizeof))();
		}
	}
	return s1.length < s2.length ? -1 : (s1.length > s2.length);
}

// Compiler hook into the runtime implementation of array (vector) operations.
template _arrayOp(Args...) {
	import core.internal.arrayop;
	alias _arrayOp = arrayOp!Args;
}

/// Provide the .dup array property.
@property auto dup(T)(T[] a)
	if (!is(const(T) : T)) {
	import core.internal.traits : Unconst;
	static assert(is(T : Unconst!T), "Cannot implicitly convert type "~T.stringof~
					" to "~Unconst!T.stringof~" in dup.");

	// wrap unsafe _dup in @trusted to preserve @safe postblit
	static if (__traits(compiles, (T b) @safe { T a = b; }))
		return _trustedDup!(T, Unconst!T)(a);
	else
		return _dup!(T, Unconst!T)(a);
}

/// ditto
// const overload to support implicit conversion to immutable (unique result, see DIP29)
@property T[] dup(T)(const(T)[] a)
	if (is(const(T) : T)) {
	// wrap unsafe _dup in @trusted to preserve @safe postblit
	static if (__traits(compiles, (T b) @safe { T a = b; }))
		return _trustedDup!(const(T), T)(a);
	else
		return _dup!(const(T), T)(a);
}


/// Provide the .idup array property.
@property immutable(T)[] idup(T)(T[] a) {
	static assert(is(T : immutable(T)), "Cannot implicitly convert type "~T.stringof~
					" to immutable in idup.");

	// wrap unsafe _dup in @trusted to preserve @safe postblit
	static if (__traits(compiles, (T b) @safe { T a = b; }))
		return _trustedDup!(T, immutable(T))(a);
	else
		return _dup!(T, immutable(T))(a);
}

/// ditto
@property immutable(T)[] idup(T:void)(const(T)[] a) {
	return a.dup;
}

private U[] _trustedDup(T, U)(T[] a) @trusted {
	return _dup!(T, U)(a);
}

private U[] _dup(T, U)(T[] a)  { // pure nothrow depends on postblit
	if (__ctfe) {
		static if (is(T : void))
			assert(0, "Cannot dup a void[] array at compile time.");
		else {
			U[] res;
			foreach (ref e; a)
				res ~= e;
			return res;
		}
	}

	import core.memops : memcpy;

	void[] arr = _d_newarrayU(typeid(T[]), a.length);
	memcpy(arr.ptr, cast(const(void)*)a.ptr, T.sizeof	* a.length);
	auto res =	*cast(U[]*)&arr;

	static if (!is(T : void))
		_doPostblit(res);
	return res;
}

private extern (C) void[] _d_newarrayU(const TypeInfo ti, size_t length) pure nothrow;


/**************
	* Get the postblit for type T.
	* Returns:
	*		null if no postblit is necessary
	*		function pointer for struct postblits
	*		delegate for class postblits
	*/
private auto _getPostblit(T)() @trusted pure nothrow @nogc {
	// infer static postblit type, run postblit if any
	static if (is(T == struct)) {
		import core.internal.traits : Unqual;
		// use typeid(Unqual!T) here to skip TypeInfo_Const/Shared/...
		alias _PostBlitType = typeof(function (ref T t){ T a = t; });
		return cast(_PostBlitType)typeid(Unqual!T).xpostblit;
	}
	else if ((&typeid(T).postblit).funcptr !is &TypeInfo.postblit) {
		alias _PostBlitType = typeof(delegate (ref T t){ T a = t; });
		return cast(_PostBlitType)&typeid(T).postblit;
	}
	else
		return null;
}

private void _doPostblit(T)(T[] arr) {
	// infer static postblit type, run postblit if any
	if (auto postblit = _getPostblit!T()) {
		foreach (ref elem; arr)
			postblit(elem);
	}
}
