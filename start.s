	.file "start.s"

	.ifdef LANG_C
	.set LANG_C, 1
	.else
	.set LANG_C, 0
	.endif
	.ifdef LANG_D
	.set LANG_D, 1
	.else
	.set LANG_D, 0
	.endif
	.ifdef PIC
	.set PIC, 1
	.else
	.set PIC, 0
	.endif

	.section	.text._start,"ax",@progbits
	.p2align 4,,15

	.globl	_start
	.type	_start, @function
_start:
	// If C, then call constructors, main, and destructors
 .if LANG_C || LANG_D
	mov 0(%rsp), %r12
	lea 8(%rsp), %r13

 .if LANG_D
	// Get argument lengths
	// Allocate space for the array
	mov %r12, %rax
	shl $0x3, %rax
	// Save half - needed for args loop
	mov %rax, %r14
	shl $0x1, %rax
	sub %rax, %rsp
	// Loop through args
	xor %r15, %r15
 .Lloop_args:
	mov 0(%r13,%r15), %rdi
	mov %rdi, 8(%rsp,%r15,2)
	call strlen
	mov %rax, 0(%rsp,%r15,2)
	add $0x8, %r15
	cmp %r15, %r14
	jne .Lloop_args
 .Ldone_args:
	// Replace old C-style args ptr with D-style args ptr
	mov %rsp, %r13
 .endif

	// Loop through
	.if PIC
	lea __sec_init_array_beg(%rip), %r14
	lea __sec_init_array_end(%rip), %r15
	.else
	lea __sec_init_array_beg, %r14
	lea __sec_init_array_end, %r15
	.endif
	cmp %r14, %r15
	je .Ldone_inits
 .Lloop_inits:
	mov %r12, %rdi
	mov %r13, %rsi
	call *(%r14)
	add $0x8, %r14
	cmp %r14, %r15
	jne .Lloop_inits
 .Ldone_inits:
	// Call main
	mov %r12, %rdi
	mov %r13, %rsi
 .if LANG_D
	call _Dmain
 .else
	call main
 .endif
	// Save return and loop destructors
	mov %rax, %rbx
	.if PIC
	lea __sec_fini_array_beg(%rip), %r14
	lea __sec_fini_array_end(%rip), %r15
	.else
	lea __sec_fini_array_beg, %r14
	lea __sec_fini_array_end, %r15
	.endif
	cmp %r14, %r15
	je .Ldone_finis
 .Lloop_finis:
	mov %r12, %rdi
	mov %r13, %rsi
	mov %rbx, %rdx
	call *(%r14)
	add $0x8, %r14
	cmp %r14, %r15
	jne .Lloop_finis
 .Ldone_finis:
	// Move return to parameter for syscall
	mov %rbx, %rdi
 .else
	// Otherwise, simply call main and exit with return value.
	mov 0(%rsp), %rdi
	lea 0(%rsp), %rsi
	call main
	mov %rax, %rdi
 .endif
	// Exit
	mov $60,  %rax
	syscall
	.size	_start, .-_start
