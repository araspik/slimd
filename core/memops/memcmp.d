/**** Compares two blocks of memory.
  * 
	* This module provides functions for comparing two memory blocks.
	* It utilizes void16 whenever possible.
	* 
	* Authors: ARaspiK
	* Date: Jun 12, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.memops.memcmp;

import gcc.attribute;

@nogc @trusted nothrow pure:

/**** Compares two blocks of memory.
	* 
	* It utilizes the best SIMD extension set available to compare the given blocks of memory quickly.
	* It is callable from C code, and is completely compatible as such.
	* It also provides a CTFE implementation.
	*/
extern(C) @nogc @trusted int memcmp(const(void)* a, const(void)* b, size_t len) pure nothrow {
	if (__ctfe) {
		auto aA = cast(const(ubyte)*)a, bA = cast(const(ubyte)*)b;
		for (uint i = 0; i < len; i++)
			if (aA[i] != bA[i])
				return aA[i] - bA[i];
		return 0;
	}

	//static __gshared immutable @nogc @trusted nothrow pure
		//int function(const(void)*, const(void)*, size_t) impl = &memcmp_SSE2;
	//return impl(a, b, len);

	return memcmp_SSE2(a, b, len);
}

///
pure unittest {
	string a = "abc", b = "abd";
	assert (memcmp(a.ptr, b.ptr, 3) < 0);
	assert (memcmp(b.ptr, a.ptr, 3) > 0);
	assert (memcmp(a.ptr, a.ptr, 3) == 0);
}

private:

/**** Compares two blocks of memory using SSE instructions.
	* 
	* Author: Agner Fog (translated to D by ARaspiK)
	* Copyright: (c) 2008-2016
	* License: GNU General Public License www.gnu.org/licenses
	*/
@attribute("target", "sse2") int memcmp_SSE2(const(void)* a, const(void)* b, size_t len) {
	import core.bitops: bsf;
	import core.simd;
	import std.ctfe: AliasSeq;

	@nogc @trusted ushort cmpSSE(T)(T a, T b) nothrow pure {
		int res;
		// In order to support different sizes, the ASM has to be done here, without using the void16
		// type, which requires the 16-byte size.
		asm @trusted @nogc nothrow pure {
			"pcmpeqb %[a], %[b]" : [b] "+x" b : [a] "x" a;
			"pmovmskb %[b], %[r]" : [r] "=r" res : [b] "x" b;
		}
		return cast(ushort)res;
	}

	a += len; b += len;
	
	len = -len;
	
	if (len == 0) return 0;

	uint r;

	while (len <= -16) {
		r = ~cmpSSE(*cast(ubyte16*)(a+len), *cast(ubyte16*)(b+len));
		if (r != 0) goto calc;
		if ((len += 16) == 0) return 0;
	}

	foreach (T; AliasSeq!(ulong, uint)) {
		if (len <= -(T.sizeof)) {
			r = ~cmpSSE(*cast(T*)(a+len), *cast(T*)(b+len));
			if (r != 0) goto calc;
			if ((len += T.sizeof) == 0) return 0;
		}
	}

	if (len <= -2) {
		r = *cast(ushort*)(a+len) - *cast(ushort*)(b+len);
		if (r != 0) {
			if (cast(ubyte)r == 0) len++;
			goto calcNB;
		}
		if ((len += 2) == 0) return 0;
	}

	goto calcNB;

 calc:
	len += r.bsf();
 calcNB:
	ubyte diffA = *cast(ubyte*)(a + len), diffB = *cast(ubyte*)(b + len);
	return diffA - diffB;
}
