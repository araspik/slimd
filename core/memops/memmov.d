/**** Copies one block of memory somewhere else, where both blocks may overlap.
  * 
	* This module provides functionality for copying blocks of data from one place in memory to
	* another, and provides support for overlapping blocks.
	* 
	* Authors: ARaspiK
	* Date: Jun 20, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.memops.memmov;

import gcc.attribute;

@nogc @trusted nothrow:

@attribute("noinline") extern(C) void memmov(void* d, const(void)* s, size_t len) {
	if (__ctfe) {
		auto src = cast(const(ubyte)[])s[0 .. len];
		auto dst = cast(ubyte[])d[0 .. len];
		if (s <= d && s >= d + len)
			foreach_reverse (size_t i, ref b; dst)
				b = src[i];
		else
			foreach (size_t i, ref b; dst)
				b = src[i];

		return;
	}

	//static immutable __gshared @nogc @trusted nothrow
		//void function(void*, const(void)*, size_t) impl = &memmov_SSE2U;
	//impl(d, s, len);

	memmov_SSE2U(d, s, len);
}

private:

/**** Copies two blocks of memory using SSE 2 instructions, via unaligned loads.
	* 
	* This is based of the asmlib's memmove64.asm: memmoveU function.
	* 
	* It calls memcpy if copying forward.
	* 
	* Authors: Agner Fog (translated to D by ARaspiK)
	* Copyright: (c) 2008-2016
	* License: GNU General Public License www.gnu.org/licenses
	*/
@attribute("forceinline") @attribute("target", "sse2")
void memmov_SSE2U(void* dstP, const(void)* srcP, size_t len) {
	import std.ctfe: AliasSeq;
	import gcc.builtins;
	import core.simd;
	import core.memops: memcpy;

	if (dstP >= srcP + len) {
		// No overlap, forwards.
		memcpy(dstP, srcP, len);
		return;
	}

	@attribute("forceinline") void cpy(T)() {
		len -= T.sizeof;
		static if (is(T == void16))
			__builtin_ia32_storeups(cast(float*)__builtin_assume_aligned(dstP + len, 16),
					__builtin_ia32_loadups(cast(float*)(srcP + len)));
		else
			*cast(T*)(dstP + len) = *cast(T*)(srcP + len);
	}

	if (len < 64) {
		if (len & 32) {
			cpy!void16;
			cpy!void16;
		}

		foreach (T; AliasSeq!(void16, ulong, uint, ushort, ubyte))
			if (len & T.sizeof) cpy!T;

		return;
	}

	// Strip off end of dst, so that end is aligned on 32-byte block
	auto dstE = cast(ptrdiff_t)(dstP + len) & 0xf;
	if (dstE != 0) {
		if (dstE & 0b11) {
			if (dstE & 0b01) cpy!ubyte;
			if (dstE & 0b10) cpy!ushort;
		}
		if (dstE & 4) cpy!uint;
		if (dstE & 8) cpy!ulong;
	}

	auto lenRemain = cast(ptrdiff_t)len & 0x1f;
	
	if (len &= -32) { // Rounded down to 32-byte blocks
		dstP += lenRemain;
		srcP += lenRemain;

		// It should perform aligned destination moves now

		/+ // Check need for non-temporal store loop
		if (len > <CACHEBYPASSLIMIT>) {
			...
		}
		+/

		do {
			assert ((len & 0xf) == 0);
			assert ((cast(ptrdiff_t)dstP & 0xf) == 0);
			cpy!void16;
			assert ((cast(ptrdiff_t)dstP & 0xf) == 0);
			cpy!void16;
		} while (len);

		dstP -= lenRemain;
		srcP -= lenRemain;
	}

	if ((len = lenRemain) != 0)
		foreach (T; AliasSeq!(void16, ulong, uint, ushort, ubyte))
			if (len & T.sizeof) cpy!T;
	auto dst = cast(ubyte[])dstP[0..len], src = cast(ubyte[])srcP[0..len];
	dst[] = src[];
	//foreach_reverse (size_t i, ref d; dst)
		//d = src[i];
}
