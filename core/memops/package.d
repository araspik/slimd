/**** Common performance-enhanced memory operations.
  * 
  * This module provides common memory functionality we usually depend on the C library for. It
	* is based on $(LINK2 http://www.agner.org,Agner Fog)'s
	* $(LINK2 http://www.agner.org/optimize/asmlib.zip,asmlib) and has been checked to compile to
	* equivalent assembly with $(LINK2 http://gdcproject.org,GDC) at optimization `-O3`.
	* 
	* Authors: ARaspiK
	* Date: May 24, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.memops;

public import core.memops.memcmp,
							core.memops.memcpy,
							core.memops.memmov;
