/**** Copies one block of memory somewhere else.
  * 
	* This module provides functionality for copying blocks of data from one place in memory to
	* another.
	* 
	* Authors: ARaspiK
	* Date: Jun 18, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.memops.memcpy;

import gcc.attribute;

@nogc @trusted nothrow:

pragma(mangle, "memcpy")
@attribute("flatten") extern(C) void memcpy(void* d, const(void)* s, size_t len) {
	if (__ctfe) {
		(cast(ubyte*)d)[0 .. len] = (cast(ubyte*)s)[0 .. len];
		return;
	}

	//static immutable __gshared @nogc @trusted nothrow
		//void function(void*, const(void)*, size_t) impl = &memcpy_SSE2;
	//impl(d, s, len);

	memcpy_SSE2(d, s, len);
}

private:

/**** Copies two blocks of memory using SSE 2 instructions.
	* 
	* This is based of the asmlib's memcpy64.asm: memcpyU function.
	* 
	* Author: Agner Fog (translated to D by ARaspiK)
	* Copyright: (c) 2008-2016
	* License: GNU General Public License www.gnu.org/licenses
	*/
@attribute("target", "sse2") void memcpy_SSE2(void* dst, const(void)* src, size_t len) {
	import std.ctfe: AliasSeq;
	import gcc.builtins;
	import core.simd;

	@attribute("forceinline") @nogc @trusted void cpy(T)() nothrow {
		static if (is(T == void16))
			__builtin_ia32_storeups(cast(float*)__builtin_assume_aligned(dst, 16),
					__builtin_ia32_loadups(cast(float*)src));
		else *cast(T*)dst = *cast(T*)src;
		dst += T.sizeof;
		src += T.sizeof;
	}

	// Copies blocks of 1-2-4-8, aligning the result by 16 bytes
	@attribute("forceinline") @nogc @trusted void align16(size_t size) {
		if (__builtin_expect(size & 0b11, 0b11)) {
			if (__builtin_expect(size & 1, 1)) cpy!ubyte;
			if (__builtin_expect(size & 2, 2)) cpy!ushort;
		}
		if (__builtin_expect(size & 4, 4)) cpy!uint;
		if (__builtin_expect(size & 8, 8)) cpy!ulong;
	}

	if (len < 64) {
		align16(len);
		if (len & 16) cpy!void16;
		if (len & 32) {
			cpy!void16;
			cpy!void16;
		}
		return;
	}

	align16((-cast(ptrdiff_t)dst) & 0xf);

	auto tmp = len;
	tmp -= (len &= -0x20);
	dst += len; src += len;

	for (size_t i = -len; i != 0; i += 32) {
		// *cast(void16*)(...) = ...
		// Causes compiler to assume src is aligned (?!!!) on my machine
		__builtin_ia32_storeups(cast(float*)__builtin_assume_aligned(dst + i, 16),
				__builtin_ia32_loadups(cast(float*)(src + i)));
		__builtin_ia32_storeups(cast(float*)__builtin_assume_aligned(dst + i + 16, 16),
				__builtin_ia32_loadups(cast(float*)(src + i + 16)));
	}

	len = 0;

	foreach (T; AliasSeq!(void16, ulong, uint, ushort, ubyte))
		if (tmp & T.sizeof) cpy!T;
}
