/** Bit operation intrinsics.
  * 
  * This module provides several bit operation intrinsics and compile-time equivalents for
	* executing bit operations (such as bit searches, bit testing, etc.) fast.
	* 
	* Authors: ARaspiK
	* Date: May 27, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.bitops;

public import core.bitops.scanning, core.bitops.testing;
