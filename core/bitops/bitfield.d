/**** Bit field support.
  * 
  * This module provides a special type for maintaining bit fields.
	* 
	* Authors: ARaspiK
	* Date: Jul 23, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.bitops.bitfield;

import std.math.basics: isPowerOfTwo;
import std.math.bigint;

private template totalLength(Args...)
		if (Args.length >= 3 && Args.length % 3 == 0) {
	static if (Args.length == 3)
		enum totalLength = Args[2];
	else
		enum totalLength = Args[2] + totalLength!(Args[3..$]);
}

private template totalName(Args...)
		if (Args.length >= 3 && Args.length % 3 == 0) {
	static if (Args.length == 3)
		enum totalName = Args[1];
	else
		enum totalName = "_" ~ Args[1] ~ totalName!(Args[3.. $]);
}

private mixin template bitfieldFields(alias var, size_t off, T, string name,
			size_t bits, Args...)
		if (Args.length % 3 == 0) {
	static if (name.length > 0) {
		static if (bits == 0) {
			mixin("enum " ~ T.stringof ~ " " ~ name ~ " = T.init;");
		} else {
			mixin("@property @trusted @nogc nothrow T " ~ name ~ "() const pure {"
					~ "return cast(T)(var & (((~0UL) >> (64 - bits)) << off));}");
			mixin("@property @trusted @nogc nothrow void " ~ name ~ "(T v) pure {"
					~ "var = cast(typeof(var))((var &	~((~0UL >> (64 - bits)) << off)) | (cast(ulong)v << off));}");
		}
	}
	static if (Args.length >= 3)
		mixin bitfieldFields!(var, off + bits, Args);
}

mixin template bitfield(Args...)
		if ((Args.length >= 3 && Args.length % 3 == 0)
		 && isPowerOfTwo(totalLength!Args) && totalLength!Args <= 64
		 && totalLength!Args >= 8) {
	
	import std.math.bigint;
	mixin("private uintX!(totalLength!Args) " ~ totalName!Args ~ ";");

	mixin bitfieldFields!(mixin(totalName!Args), 0, Args);
}
