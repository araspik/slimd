/** Bit testing techniques.
  * 
	* This module provides functions for testing bits in values.
	* 
	* Authors: ARaspiK
	* Date: June 19, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.bitops.testing;

import std.ctfe: isIntegral;

/** Returns the value of the given bit within the given type.
	* 
	*/
@nogc @safe bool bitTest(T)(T data, T bit) nothrow pure
		if (isIntegral!T && T.sizeof >= 2) {

	bool res;

	asm @nogc @trusted nothrow pure {
		"bt %[b], %[n]" : "=@ccc" res : [b] "ri" bit, [n] "rm" data : "cc";
	}

	return res;
}
