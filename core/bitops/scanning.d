/** Bit scanning intrinsics.
  * 
	* This module provides functions for scanning bits forwards and backwards.
	* 
	* Authors: ARaspiK
	* Date: June 13, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.bitops.scanning;

// Actual executor: Contains both CTFE and assembly versions.
private @nogc @trusted T bitScan(bool fwd, T)(T num) nothrow pure
		if (__traits(isIntegral, T)) {
	if (__ctfe) {
		enum bits = T.sizeof * 8;
		static if (fwd) {
			foreach (T i; 0..bits)
				if (num & (cast(T)1 << i)) return i;
		} else {
			foreach_reverse (T i; 0..bits)
				if (num & (cast(T)1 << i)) return i;
		}
		return 0;
	}

	version (GNU) {
		T res = void;
		static if (fwd)
			asm @nogc @trusted nothrow pure {"bsf %[n], %[r]" : [r] "=r" res : [n] "rm" num;}
		else
			asm @nogc @trusted nothrow pure {"bsr %[n], %[r]" : [r] "=r" res : [n] "rm" num;}
		return res;
	} else {
		return __asm!T(`bs` ~ [(fwd) ? 'f' : 'r'] ~ ` $1, $0`, `=r,rm`, res, num);
	}
}

/** Scans and returns the first set bit, in normal (least to most significant)
	* order.
	*
	* This function uses assembly in normal (not CTFE) mode, and it manually scans bits
	* (using the AND operator) in CTFE mode.
	*/
@nogc @trusted T bsf(T)(T num) nothrow pure {return num.bitScan!true;}

///
@nogc @safe unittest {
	// 0-indexed.
	assert (bsf(3) == 0);
	// BS(F|R) with input 1 << n will result in n.
	assert (bsf(1 << 3) == 3);
	assert (bsf(1 << 20) == 20);
	assert (bsf(1 << 30) == 30);
	// UFCS is helpful.
	assert (3.bsf() == 0);
	assert (319.bsf() == 0);
}

/** Scans and returns the first set bit, in reverse (most to least significant)
	* order.
	*
	* This function uses assembly in normal (not CTFE) mode, and it manually scans bits
	* (using the AND operator) in CTFE mode.
	*/
@nogc @trusted T bsr(T)(T num) nothrow pure {return num.bitScan!false;}

///
@nogc @safe unittest {
	// 0-indexed.
	assert (bsr(3) == 1);
	// BS(F|R) with input 1 << n will result in n.
	assert (bsr(1 << 3) == 3);
	assert (bsr(1 << 20) == 20);
	assert (bsr(1 << 30) == 30);
	// UFCS is helpful.
	assert (3.bsr() == 1);
	assert (319.bsr() == 8);
}
