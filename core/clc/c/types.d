/** C types.
  * 
	* This module provides types provided in C that do not have built-in replacements.
	* 
	* Authors: ARaspiK
	* Date: Jul 6, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.clc.c.types;

import gcc.builtins;

alias c_long = __builtin_clong;
alias c_ulong = __builtin_culong;

alias c_long_double = real;

alias c_str = immutable(char)*;
