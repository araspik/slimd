/**** C string length calculator.
  * 
	* This module provides functionality for retrieving the length of C strings.
	* 
	* Authors: ARaspiK
	* Date: Jul 12, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.clc.c.strlen;

import core.clc.c.types;
import core.simd;
import core.bitops;
import gcc.attribute;

@trusted @nogc pure nothrow:

extern(C) size_t strlen(const c_str data) {
	size_t off = cast(size_t)data & 0xf;
	ubyte16* adata = cast(ubyte16*)(cast(ptrdiff_t)data & -0x10);
	immutable ubyte16 zero;
	ushort t = (cast(ushort)((zero.equals(*adata).toMask!ushort
			>> off) << off)).bsf;

	for (; t == 0; adata++)
		t = zero.equals(*adata).toMask!ushort.bsf;

 done:
	return cast(size_t)adata - cast(size_t)data + t;
}

T to(T: string, F: c_str)(const F data) {
	return data[0 .. strlen(data)];
}
