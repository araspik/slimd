/**** C-to-D type converter.
  * 
	* This module provides functionality for converting between C and D types.
	* 
	* Authors: ARaspiK
	* Date: Jul 17, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.clc.c.conv;

import core.clc.c.types;
import core.clc.c.strlen;
import gcc.attribute;

@attribute("forceinline") @trusted @nogc pure nothrow:

/// Converts C strings to D strings.
T to(T: string, F: c_str)(const F data) {
	return data[0 .. strlen(data)];
}

/// Converts D strings to C strings.
T to(T: c_str, F: string)(const F data) {
	import std.alloc: alloca;
	auto space = alloca!char(data.length + 1);
	space[0 .. $-1] = data[0 .. $];
	space[$-1] = '\0';
	return cast(c_str)space.ptr;
}
