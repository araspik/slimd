/** Functions and types for interfacing with C.
  * 
	* This module provides types and functions from C or to be used to convert between D and C types.
	* 
	* Authors: ARaspiK
	* Date: Jul 9, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.clc.c;

public import core.clc.c.types,
							core.clc.c.conv,
							core.clc.c.strlen;
