/**** Checked integer arithmetic provider.
  * 
	* This module provides checked integer arithmetics.
	* 
	* Authors: ARaspiK
	* Date: Jul 22, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.checkedint;

import gcc.builtins;
import gcc.attribute;
import std.ctfe;

@trusted @nogc nothrow:

private enum op2str = [
	"+": "add",
	"-": "sub",
	"*": "mul",
];

template checkedOp(string op, T)
		if (isIntegral!T && (op in op2str) !is null) {
	@attribute("forceinline") bool checkedOp(const T a, const T b, ref T res) pure {
		return mixin("__builtin_" ~ op2str[op] ~ "_overflow(a, b, &res)");
	}
}

template willOverflow(string op, T)
		if (isIntegral!T && (op in op2str) !is null) {
	bool willOverflow(const T a, const T b) pure {
		return mixin("__builtin_" ~ op2str[op] ~ "_overflow_(a, b, cast(T)0)");
	}
}
