/**** Floating-point format distinction.
  * 
	* This module provides information about floating-point types.
	* 
	* Authors: ARaspiK
	* Date: Jun 8, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.math.floatinfo;

@nogc @safe pure nothrow:

enum FloatFormat {
	ieeeHalf,
	ieeeSingle,
	ieeeDouble,
	ieeeExtended,
	ieeeExtended53,
	ieeeQuadruple,
	ibmExtended,
}

struct FloatInfo(T)
		if (isFloating!T) {

	/// Float binary format
	FloatFormat format;
	/// Mask to select exponent (without sign) in its ushort.
	ushort expMask;
	/// Bits by which the exponent is left-shifted.
	size_t expShift;
	/// Ushort index of the exponent.
	size_t expIndex = 0;
	/// Bits by which the sign is left-shifted.
	size_t signShift = 7;
	/// Byte index of the sign.
	size_t signIndex = 0;
	/// Exponent bias - 1 (exp == exp_bias yields x2^-1).
	ushort expBias;
	/// Index of least-significant-byte of mantissa
	size_t mantissaLSB = 0;
	/// Index of most-significant-byte of mantissa.
	size_t mantissaMSB = 1;

 @nogc @safe pure nothrow:

	/**** Returns whether the mantissa of the given floating-point type is non-zero or not.
		* 
		*/
	@trusted bool mantissaNonZero(const T val) const {
		switch (format) {
			case FloatFormat.ieeeSingle:
				return (*(cast(uint*)&val) & 0x007f_ffff) != 0;
			case FloatFormat.ieeeDouble:
				return (*(cast(ulong*)&val) & 0x000f_ffff_ffff_ffff) != 0;
			case FloatFormat.ieeeExtended:
				return (*(cast(ulong*)&val) & 0x0000_ffff_ffff_ffff) != 0;
			case FloatFormat.ieeeQuadruple:
				return ((cast(ulong*)&val)[mantissaLSB]
						| ((cast(ulong*)&val)[mantissaMSB] & 0x0000_ffff_ffff_ffff)) != 0;
			case FloatFormat.ibmExtended:
				return ((cast(ulong*)&val)[mantissaLSB]
						| ((cast(ulong*)&val)[mantissaMSB] & 0x000f_ffff_ffff_ffff)) != 0;
			default: return false;
		}
	}

	@trusted ushort getExponent(const T val) const {
		return expMask & (cast(ushort*)&val)[expIndex];
	}
}

FloatInfo!T floatInfo(T)()
		if (isFloating!T) {
	FloatInfo!T res;
	version (BigEndian) {
		res.mantissaLSB = 1;
		res.mantissaMSB = 0;
	}
	switch (T.mant_dig) {
		case 24:
			res.format = FloatFormat.ieeeSingle;
			res.expMask = 0x7f80;
			res.expShift = 7; // 0b1000_0000 = 0x80
			res.expBias = 0x3f00;
			version (LittleEndian) {
				res.expIndex = 1;
				res.signIndex = 3;
			}
			break;
		case 53:
			if (T.sizeof == 8) {
				res.format = FloatFormat.ieeeDouble;
				res.expMask = 0x7ff0;
				res.expShift = 4;
				res.expBias = 0x3fe0;
				version (LittleEndian) {
					res.expIndex = 3;
					res.signIndex = 7;
				}
			} else if (T.sizeof == 12) {
				res.format = FloatFormat.ieeeExtended53;
				res.expMask = 0x7fff;
				res.expShift = 0;
				res.expBias = 0x3ffe;
				version (LittleEndian) {
					res.expIndex = 4;
					res.signIndex = 9;
				}
			}
			break;
		case 64:
			res.format = FloatFormat.ieeeExtended;
			res.expMask = 0x7fff;
			res.expShift = 0;
			res.expBias = 0x3ffe;
			version (LittleEndian) {
				res.expIndex = 4;
				res.signIndex = 9;
			}
			break;
		case 113:
			res.format = FloatFormat.ieeeQuadruple;
			res.expMask = 0x7fff;
			res.expShift = 0;
			res.expBias = 0x3ffe;
			version (LittleEndian) {
				res.expIndex = 7;
				res.signIndex = 15;
			}
			break;
		case 106:
			res.format = FloatFormat.ibmExtended;
			res.expMask = 0x7ff0;
			res.expShift = 4;
			res.expBias = 0x3fe0;
			version (LittleEndian) {
				res.expIndex = 7;
				res.signIndex = 15;
			}
			break;
		default: break;
	}
	return res;
}
