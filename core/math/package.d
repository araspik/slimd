/**** Provides internal, compiler-close, and system-dependent math functionality.
  * 
	* This package provides intrinsics, binary format information, intrinsics, etc. for use in a
	* system-dependent manner.
	* 
	* Authors: ARaspiK
	* Date: Jul 9, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.math;

public import core.math.division,
							core.math.floatinfo,
							core.math.intrinsics;
