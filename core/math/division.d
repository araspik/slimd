/**** Provides fast division number calculation.
  * 
	* This module provides functionality from Agner Fog's asmlib (divfixedi64.asm) to calculate divisions
	* fast.
	* 
	* Authors: ARaspiK
	* Date: Jun 26, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.math.division;

struct QDivInfo {
	int a, b;
}

