/**** Intrinsics for math functions.
  * 
  * This module contains intrinsics for aiding in performing math operations.
	* 
	* Authors: ARaspiK
	* Date: May 27, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.math.intrinsics;
