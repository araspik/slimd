/**** Hash function implementation.
	* 
	* Authors: ARaspiK
	* Date: Jul 10, 2018
	* License: MIT
	*/
module core.hash;

/**** Implementation of Paul Hseih's SuperFastHash algo, with seed-hash chaining.
	* 
	* Authors: Sean Kelly (rewrite by ARaspiK)
	* License: Boost
	*/
@trusted @nogc size_t hashOf(const(void)[] buf, size_t seed) pure nothrow {

	auto data = (cast(const(ubyte)*)buf.ptr)[0 .. buf.length];
	auto hash = seed;

	@trusted @nogc uint get16() nothrow {
		ushort res;
		if (__ctfe) res = (cast(const(ushort)[])data)[0];
		else res = *cast(const(ushort)*)data.ptr;
		data = data[2..$];
		return res;
	}

	if (data.length == 0 || data.ptr is null) return 0;

	while (data.length >= 4) {
		hash += get16;
		hash = (hash << 16) ^ ((get16 << 11) ^ hash);
		hash += hash >> 11;
	}

	switch (data.length) {
		case 3:
			hash += get16;
			hash = (hash ^ (hash << 16)) ^ (data[0] << 18);
			hash += hash >> 11;
			break;
		case 2:
			hash += get16;
			hash ^= hash << 11;
			hash += hash >> 17;
			break;
		case 1:
			hash += data[0];
			hash ^= hash << 10;
			hash += hash >> 1;
			break;
		default:
			import gcc.builtins;
			__builtin_unreachable();
	}

	hash ^= hash << 3;
	hash += hash >> 5;
	hash ^= hash << 4;
	hash += hash >> 17;
	hash ^= hash << 25;
	hash += hash >> 6;

	return hash;

}
