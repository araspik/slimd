/**** Checks whether SIMD instructions for unaligned reads are fast.
  * 
	* Checks whether SIMD instructions (SSE / AVX) have faster unaligned reads than an alignment shift and
	* aligned read.
	* 
	* Authors: ARaspiK
	* Date: June 20, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.simd.goodunaligned;

import core.cpuid;
import core.simd;

/**** Checks if unaligned reads are faster than force-aligning reads for the given SIMD type.
	* 
	* In many cases, aligning unaligned data and then performing reads may be faster than reading
	* unaligned. This function figures it out for the current CPU.
	* 
	* Authors: Agner Fog (translated by ARaspiK)
	* License: GNU General Public License
	* Copyright: (c) 2011-2013
	*/
@nogc @safe bool fastUnalignedReads(T : void16)() nothrow {
	switch (cpu.vendor) {
		
		case Vendor.intel:
			if (cpu.family < 6) return false; // Old Pentium 1, etc. models.
			if (cpu.family == 0xf) return false; // Old Netburst architecture
			if (cpu.model < 0x1a) return false; // Earlier than Nehalem
			if (cpu.model == 0x1c) return false; // Intel Atom
			return true;

		case Vendor.amd:
			if (cpu.family < 0x10) return false; // AMD K8 or earlier
			if (cpu.family == 0x16) return false; // Jaguar: use aligned
			return true;

		case Vendor.via:
			if (cpu.family <= 0xf) return false; // VIA Nano
			return false; // unknown

		default:
			return false;
	}
}
