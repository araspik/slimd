/**** Builtin SIMD intrinsics.
  * 
	* This module provides compiler intrinsics for SIMD instructions and types.
	* 
	* Authors: ARaspiK
	* Date: Jul 9, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.simd;

public import core.simd.goodunaligned;

pure nothrow @safe @nogc:

/**** SIMD vector types.
	*
	* Parameters:
	*      T = one of double[2], float[4], void[16], byte[16], ubyte[16],
	*      short[8], ushort[8], int[4], uint[4], long[2], ulong[2].
	*      For 256 bit vectors,
	*      one of double[4], float[8], void[32], byte[32], ubyte[32],
	*      short[16], ushort[16], int[8], uint[8], long[4], ulong[4]
	*/
alias Vector(T) = __vector(T);

alias void8 = Vector!(void[8]);
alias bool8 = Vector!(bool[8]);
alias float2 = Vector!(float[2]);
alias byte8 = Vector!(byte[8]);
alias ubyte8 = Vector!(ubyte[8]);
alias short4 = Vector!(short[4]);
alias ushort4 = Vector!(ushort[4]);
alias int2 = Vector!(int[2]);
alias uint2 = Vector!(uint[2]);
alias void16 = Vector!(void[16]);
alias bool16 = Vector!(bool[16]);
alias double2 = Vector!(double[2]);
alias float4 = Vector!(float[4]);
alias byte16 = Vector!(byte[16]);
alias ubyte16 = Vector!(ubyte[16]);
alias short8 = Vector!(short[8]);
alias ushort8 = Vector!(ushort[8]);
alias int4 = Vector!(int[4]);
alias uint4 = Vector!(uint[4]);
alias long2 = Vector!(long[2]);
alias ulong2 = Vector!(ulong[2]);
alias void32 = Vector!(void[32]);
alias bool32 = Vector!(bool[32]);
alias double4 = Vector!(double[4]);
alias float8 = Vector!(float[8]);
alias byte32 = Vector!(byte[32]);
alias ubyte32 = Vector!(ubyte[32]);
alias short16 = Vector!(short[16]);
alias ushort16 = Vector!(ushort[16]);
alias int8 = Vector!(int[8]);
alias uint8 = Vector!(uint[8]);
alias long4 = Vector!(long[4]);
alias ulong4 = Vector!(ulong[4]);

/**** Compares two SSE sets.
	* 
	* It performs a element-by-element comparison and returns an integral vector of the same length
	* containing 1 (equals) or 0 (not equals) per element.
	*/
auto equals(T: __vector(U[N]), U, size_t N)(T a, T b)
		if (U.sizeof * N == 16) {
	import std.ctfe, std.math.bigint;
	import gcc.builtins;

	static if (isFloating!T)
		alias R = Vector!(uintX!(U.sizeof)[N]);
	else
		alias R = T;

	static if (U.sizeof == 1) return cast(R)__builtin_ia32_pcmpeqb128(a, b);
	else static if (U.sizeof == 2) return cast(R)__builtin_ia32_pcmpeqw128(a, b);
	else static if (U.sizeof == 4) return cast(R)__builtin_ia32_pcmpeqd128(a, b);
	else static if (U.sizeof == 8) return cast(R)__builtin_ia32_pcmpeqq128(a, b);
}

import std.ctfe;

/**** Saves the first bits of each element.
	* 
	* It stores the first bit of every byte.
	*/
T toMask(T, F: __vector(U[N]), U, size_t N)(F data)
		if (U.sizeof == 1 && isIntegral!T && T.sizeof == N / 8) {
	import gcc.builtins;
	static if (N == 8) return cast(T)__builtin_ia32_pmovmskb(data);
	else static if (N == 16) return cast(T)__builtin_ia32_pmovmskb128(data);
	else static if (N == 32) return cast(T)__builtin_ia32_pmovmskb256(data);
}
