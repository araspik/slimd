/** CPU feature detection support.
  * 
	* This module provides functionality for detecting features and information of the CPU.
	* This stuff currently works in runtime only, due to lesser such detection at compile-time.
	* No function has to be called to use the information provided here - A static constructor
	* handles it.
	* 
	* Authors: ARaspiK
	* Date: Jun 19, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.cpuid;

public import core.cpuid.utils;

import core.bitops: bitTest;

/** Provides information about the current CPU.
	*/
public static shared CPU cpu;

/** Stores information about the current CPU.
	* 
	* This structure provides information to identify CPUs, as well as retrieve information about what
	* instruction sets are supported and other CPU features provided by cpuid.
	*/
struct CPU {

	/** Differentiates between vendors of different CPUs.
		* 
		* Provides distinction of CPU vendors.
		*/
	enum Vendor {
		unknown = 0,
		intel,
		amd,
		via,
		centaur = via,
		cyrix,
		nexgen,
	}

	/** Specifies the highest instruction set for AVX or SSE (older) supported.
		* 
		* Versions for these sets come consecutively one after the other, and so they can be safely
		* kept as one enum to specify all.
		*/
	enum SIMDVer {
		none = 0,
		mmx,
		sse,
		sse2,
		sse3,
		ssse3,
		sse4_1,
		sse4_2,
		avx,
		avx2,
		avx512f,
		avx512dq,
		avx512bw,
		avx512vl,
	}

	/** Specifies the current BMI set supported by the CPU.
		* 
		* BMI stands for Bit Manipulation Instruction.
		*
		* These instructions were created for faster bit manipulation.
		*/
	enum BMIVer {
		none = 0,
		bmi1,
		bmi2,
	}

	Vendor vendor;
	uint family;
	uint model;

	bool popcnt,
			 pclmul,
			 aes,
			 fma3,
			 f16c,
			 lzcnt;

	SIMDVer simdVer;
	BMIVer bmiVer;

	/** Automatically detects CPU features.
		* 
		* It calls the CPUID instruction, and performs bittests to determine feature availability.
		*/
	static @nogc @safe CPU getCPU() nothrow pure {
		CPU res;

		res.vendor = getVendor();
		res.family = getFamily();
		res.model = getModel();

		cpuidRes id1 = cpuid(1);
		res.popcnt = id1.ecx.bitTest(23);
		res.pclmul = id1.ecx.bitTest(1);
		res.aes    = id1.ecx.bitTest(25);
		res.fma3   = id1.ecx.bitTest(12);
		res.f16c   = id1.ecx.bitTest(29);
		res.lzcnt  = cpuid(0x80000001).ecx.bitTest(5);

		res.simdVer = getSIMDVer();
		res.bmiVer = getBMIVer();

		return res;
	}

	/** Utilizes code from Agner Fog's asmlib (file cputype64.asm) to retrieve CPU vendor.
		* 
		* Authors: Agner Fog
		* License: GNU General Public License
		* Copyright: (c) 2011
		*/
	static @nogc @safe Vendor getVendor() nothrow pure {
		Vendor vendor;
		cpuidRes id0 = cpuid(0);

		// Somehow, the casting shit here is optimized into a dword okay by the compiler.
				 if (id0.ecx == (cast(uint[1])cast(ubyte[4])"ntel")[0]) vendor = Vendor.intel;
		else if (id0.ecx == (cast(uint[1])cast(ubyte[4])"cAMD")[0]) vendor = Vendor.amd;
		else if (id0.ebx == (cast(uint[1])cast(ubyte[4])"Cent")[0]) vendor = Vendor.centaur;
		else if (id0.ebx == (cast(uint[1])cast(ubyte[4])"VIA ")[0]) vendor = Vendor.via;
		else if (id0.ebx == (cast(uint[1])cast(ubyte[4])"Cyri")[0]) vendor = Vendor.cyrix;
		else if (id0.ebx == (cast(uint[1])cast(ubyte[4])"NexG")[0]) vendor = Vendor.nexgen;

		return vendor;
	}

	/** Utilizes code from Agner Fog's asmlib (file cputype64.asm) to retrieve CPU family.
		* 
		* Authors: Agner Fog
		* License: GNU General Public License
		* Copyright: (c) 2011
		*/
	static @nogc @safe uint getFamily() nothrow pure {
		cpuidRes id1 = cpuid(1);
		return ((id1.eax >> 8) & 0xf) + ((id1.eax >> 20) & 0xff);
	}

	/** Utilizes code from Agner Fog's asmlib (file cputype64.asm) to retrieve CPU model.
		* 
		* Authors: Agner Fog
		* License: GNU General Public License
		* Copyright: (c) 2011
		*/
	static @nogc @safe uint getModel() nothrow pure {
		cpuidRes id1 = cpuid(1);
		return ((id1.eax >> 4) & 0xf) | ((id1.eax >> 12) & 0xf0);
	}

	/** Utilizes code from Agner Fog's asmlib (file instrset64.asm) to retrieve usable instruction sets.
		* 
		* Authors: Agner Fog
		* License: GNU General Public License
		* Copyright: (c) 2003-2018
		*/
	static @nogc @safe SIMDVer getSIMDVer() nothrow pure {
		// In 64-bit mode, we assume that SSE 2 is supported.
		SIMDVer res = SIMDVer.sse2;

		cpuidRes id1 = cpuid(1);
		
		foreach (b; [1, 9, 19, 20]) // sse3, ssse3, sse4_1, sse4_2
			if (id1.ecx.bitTest(b)) res++; else return res;

		if (id1.ecx.bitTest(27)) { // (OSXSAVE) xgetbv supported
			cpuidRes idX0 = xgetbv(0);
			if ((idX0.eax & 6) == 6 && id1.ecx.bitTest(28)) res++; else return res; // avx
		} else return res;

		cpuidRes id7 = cpuid(7, 0);

		if (id7.ebx.bitTest(5)) res++; else return res; // avx2
		
		if (id7.ebx.bitTest(16)) { // avx512f, according to cpuid
			cpuidRes idX0 = xgetbv(0);

			if ((idX0.eax & 0xe0) == 0xe0) res++; else return res; // avx512f

			foreach (b; [17, 30, 31]) // avx512{dq,bw,vl}
				if (idX0.ebx.bitTest(b)) res++; else return res;
		}

	  return res;
	}

	static @nogc @safe BMIVer getBMIVer() nothrow pure {
		BMIVer res;

		cpuidRes id7 = cpuid(7, 0);

		foreach (b; [3, 8])
			if (id7.ebx.bitTest(b)) res++; else goto done;

	 done:
		return res;
	}
}
