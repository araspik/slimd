/**** Provides functions to test for CPU features.
  * 
	* This module provides simple functions for calling CPUID and similar instructions to get information
	* on what features the CPU supports.
	* 
	* Authors: ARaspiK
	* Date: Jun 20, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.cpuid.utils;

version (LDC) import ldc.llvmasm;

/**** Defines output from a CPUID instruction.
	* 
	* This struct provides access to the registers that may be called for
	*/
struct cpuidRes {
	uint eax, ebx, ecx, edx;

	this(uint eax) {
		this = cpuid(eax);
	}

	this(uint eax, uint ecx) {
		this = cpuid(eax, ecx);
	}
}

/**** Uses CPUID to retrieve some information.
	* 
	*/
@nogc @safe cpuidRes cpuid(uint eax) pure nothrow {
	cpuidRes res;

	version (GNU) {
		asm @trusted @nogc nothrow {
			"cpuid" : "=a" res.eax, "=b" res.ebx, "=c" res.ecx, "=d" res.edx : "a" eax;
		}
	} else {
		__asm(`cpuid`, `={eax},={ebx},={ecx},={edx},{eax}`,
				res.eax, res.ebx, res.cx, res.edx, eax);
	}

	return res;
}

/// ditto
@nogc @safe cpuidRes cpuid(uint eax, uint ecx) pure nothrow {
	cpuidRes res;

	version (GNU) {
		asm @trusted @nogc nothrow {
			"cpuid" : "=a" res.eax, "=b" res.ebx, "=c" res.ecx, "=d" res.edx : "a" eax, "c" ecx;
		}
	} else {
		__asm(`cpuid`, `={eax},={ebx},={ecx},={edx},{eax},{ecx}`,
				res.eax, res.ebx, res.ecx, res.edx, eax, ecx);
	}

	return res;
}

@nogc @safe cpuidRes xgetbv(uint ecx) pure nothrow {
	cpuidRes res;

	version (GNU) {
		asm @trusted @nogc nothrow {
			"xgetbv" : "=a" res.eax, "=d" res.edx : "c" ecx;
		}
	} else {
		__asm(`xgetbv`, `={eax},={edx},{ecx}`, res.eax, res.edx, ecx);
	}

	return res;
}
