/**** Core builtin provider.
  * 
	* This module provides a nice interface for built-in functions from GCC.
	* 
	* Authors: ARaspiK
	* Date: Jul 18, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.builtins;

import gcc.builtins;
import gcc.attribute;

@attribute("forceinline") @trusted @nogc nothrow:

enum PtrBoundary {
	all,
	none
}

/**** Provides an association with GCC's Pointer Bounds Checker.
	* 
	* Parameters:
	*		p: Pointer to bound
	*		s: Number of elements forward to bound up to.
	* 
	* Returns: The pointer, with an associated boundary.
	*/
@property ref T* bounds(T)(return ref T* p, const size_t s) pure {
	return p = cast(T*)__builtin___bnd_set_ptr_bounds(p, T.sizeof * s);
}

/**** Provides an association with GCC's Pointer Bounds Checker.
	* 
	* Parameters:
	*		p: Pointer to bound
	*		p2: Pointer whose bounds to use.
	* 
	* Returns: The pointer, with the boundaries of the other given pointer.
	*/
@property ref T* bounds(T)(return ref T* p, const T* p2) pure {
	return p = cast(T*)__builtin___bnd_copy_ptr_bounds(p, p2);
}

/**** Provides an association with GCC's Pointer Bounds Checker.
	* 
	* Parameters:
	*		p: Pointer to bound
	*		t: Special type to bound with
	* 
	* Returns: The pointer, with the boundaries set by input.
	*/
@property ref T* bounds(T)(return ref T* p, const PtrBoundary t) pure {
	return p = cast(T*)(t == PtrBoundary.all ?
			__builtin___bnd_init_ptr_bounds(p) :
			__builtin___bnd_null_ptr_bounds(p));
}

/**** Provides an association with GCC's Pointer Bounds Checker.
	* 
	* Parameters:
	*		p: Pointer to bound
	*		p2: Pointer whose bounds to narrow against.
	*		s: Number of elements to intersect against.
	* 
	* Returns: The pointer, bounded by the intersection between the bounds of the
	* second pointer and the given pointer up to s elements.
	*/
ref T* bounds(T)(return ref T* p, const T* p2, const size_t s) pure {
	return p = cast(T*)__builtin___bnd_narrow_ptr_bounds(p, p2, T.sizeof * s);
}

/**** Returns the pointer bounds as an array of that type.
	* 
	* Parameters:
	*		p: Pointer to get bounds of.
	* 
	* Returns: The array for the ptr as bounds.
	*/
T[] bounds(T)(T* p) pure {
	T* pl = cast(T*)__builtin___bnd_get_ptr_lbound(p);
	T* pu = cast(T*)__builtin___bnd_get_ptr_ubound(p);
	return pl[0 .. cast(size_t)(pu - pl)];
}

/**** Checks whether the given pointer is within it's registered bounds.
	* 
	* I have no idea what this actually does. Use it as necessary.
	*/
void chkBounds(T)(T* p) pure {
	__builtin___bnd_chk_ptr_lbounds(p);
	__builtin___bnd_chk_ptr_ubounds(p);
}

/**** Checks whether the given array is within the first pointer's registered bounds.
	* 
	* I have no idea what this actually does. Use it as necessary.
	*/
void chkBounds(void[] p) {
	__builtin___bnd_chk_ptr_bounds(p.ptr, p.length);
}

/**** Returns the frame address of the current function.
	* 
	* It could return frame pointers for caller functions, but GCC warns against this,
	* so we don't support that.
	*/
void* framePtr() pure {
	return __builtin_frame_address(0);
}

/**** Returns the address where the current function returns to.
	*/
void* returnPtr() pure {
	return __builtin_extract_return_addr(__builtin_return_address(0));
}
