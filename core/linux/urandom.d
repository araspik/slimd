/**** An interface to Linux's builtin urandom PRNG.
  * 
	* This module provides access to Linux's urandom source, which can be used to both recieve a
	* random sequence of bytes and create random numbers (which is just the former fixed at
	* integer length).
	* 
	* Authors: ARaspiK
	* Date: May 27, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.linux.urandom;

import core.linux.syscall;
import std.ctfe: isIntegral;

/**** Fills a buffer with bytes from Linux's urandom source.
	* 
	* This function utilizes the Linux syscall interface to call the `getrandom` syscall,
	* passing in details about the given buffer.
	*/
@nogc @trusted void fillRandom(void[] buf) nothrow {
	@nogc @safe void sysFill(ubyte[] rBuf) nothrow {
		syscall(Syscall.getrandom, rBuf.explicitMem!(accessMode.output), rBuf.length, 0);
	}
	while (buf.length > 256) {
		sysFill(cast(ubyte[])buf[0..256]);
		buf = buf[256..$];
	}
	if (buf.length != 0)
		sysFill(cast(ubyte[])buf);
}

/**** Returns a random number acquired from Linux's urandom source.
	* 
	* This function utilizes the Linux syscall interface to call the `getrandom` syscall,
	* returning an integral data type as specified, which is by default size_t.
	*/
@nogc @safe T randomInt(T = size_t)() nothrow if (isIntegral!T) {
	T num;
	(cast(ubyte[T.sizeof])cast(T[1])num).fillRandom;
	return num;
}
