/**** A Linux Syscall interface
  * 
  * This module contains functionality to call kernel syscalls from D. It can be used to access
	* a wide variety of functionality, from performing I/O to using IPC.
	* 
	* Authors: ARaspiK
	* Date: May 24, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.linux.syscall;

public import core.linux.syscalls;

import std.ctfe: staticJoin, AliasSeq;

version (X86_64) {
	// Short names for registers as GCC understands them.
	private enum regShorts = [
		"rax": "a",
		"rbx": "b",
		"rcx": "c",
		"rdx": "d",
		"rdi": "D",
		"rsi": "S",
	];

	// The enums beyond this comment can be found for various architectures in syscall(2).
	// Add architectures as a low-priority todo or whenever requested. It should be easy.

	// Registers in which syscall arguments are taken, starting with the syscall number register
	// and ending with the syscall return register.
	private enum argRegs = [
		"rax",
		"rdi", "rsi", "rdx", "r10", "r8", "r9",
		"rax"
	];

	private enum sysReg = "rax"; // syscall number here
	// modified by syscall instruction
	private alias clbRegs = AliasSeq!("rcx","r11");
	private enum asmExe = "syscall"; // instruction to execute, in assembly
}

private template argName(size_t num) if (num > 0) {
	enum realNum = num - 1;
	enum argName = "args[" ~ realNum.stringof[0..$-2] ~ "]";
}
private template argName(size_t num) if (num == 0) {
	enum argName = "call";
}

// Converts the given arguments to lists of constraints, parameter names, and instructions.
private template argsToRegs(size_t num, T...) {
	static if (T.length == 0) {
		// Usage: asm {"<instructions>; <syscall>"
		//  	: <out_constraints><out_params>
		//		: <in_identifiers><in_constraints><in_params>
		//    : <clobbers>;}
		alias inputs = AliasSeq!();
		alias outputs = AliasSeq!();
		alias clobbers = AliasSeq!();
		alias instructs = AliasSeq!();
	} else static if (T.length == 1) {
		static assert (argRegs.length > num,
				"There are not enough applicable registers for this call!");
		static if (is(T[0] : ExplicitMemory!(M1, U1), accessMode M1, U1))
			enum vArg = "(" ~ argName!(num) ~ ".data.ptr)";
		else
			enum vArg = argName!(num);
		enum reg = argRegs[num];
		static if ((reg in regShorts) !is null) {
			alias inputs0 = AliasSeq!("[" ~ reg ~ "] \"" ~ regShorts[reg]
					~ "\" cast(size_t)" ~ vArg);
			alias clobbers = AliasSeq!();
			alias instructs = AliasSeq!();
		} else {
			alias inputs0 = AliasSeq!("[" ~ reg ~ "] \"rmi\" cast(size_t)" ~ vArg);
			alias clobbers = AliasSeq!(reg);
			alias instructs = AliasSeq!("mov %[" ~ reg ~ "], %%" ~ reg);
		}
		// When passing pointers, one can specify whether the memory pointed to is read, written
		// to, or both. This prevents us from adding the "memory" clobber, which flushes all
		// registers to memory and is inefficient.
		static if (is(T[0] : ExplicitMemory!(M, U), accessMode M, U)) {
			static if (M == accessMode.input) {
				alias inputs1 = AliasSeq!("\"m\" *" ~ vArg);
				alias outputs = AliasSeq!();
			} else static if (M == accessMode.output) {
				alias inputs1 = AliasSeq!();
				alias outputs = AliasSeq!("\"=m\" *" ~ vArg);
			} else { // M == accessMode.in_out
				alias inputs1 = AliasSeq!();
				alias outputs = AliasSeq!("\"+m\" *" ~ vArg);
			}
			alias inputs = AliasSeq!(inputs0, inputs1);
		} else {
			alias inputs = AliasSeq!(inputs0);
			alias outputs = AliasSeq!();
		}
	} else {
		alias half0 = argsToRegs!(num, T[0 .. $/2]);
		alias half1 = argsToRegs!(num + (T.length / 2), T[$/2 .. $]);
		alias inputs = AliasSeq!(half0.inputs, half1.inputs);
		alias outputs = AliasSeq!(half0.outputs, half1.outputs);
		alias clobbers = AliasSeq!(half0.clobbers, half1.clobbers);
		alias instructs = AliasSeq!(half0.instructs, half1.instructs);
	}
}

version (LDC) {
	/// Outputs the assembly constraint list.
	private char[] constraints(T...) {
		string[] outs = ["={" ~ argRegs[$-1] ~ "}"];
		string[] inps = ["{" ~ argRegs[0] ~ "}"];
		foreach (size_t i, U; T) {
			static if (is(T[0] : ExplicitMemory!(M, U), accessMode M, U)) {
				switch (M) {
					case accessMode.input:
						inps ~= "{" ~ argRegs[1 + i] ~ "}";
						break;
					case accessMode.output:
						outs ~= "={" ~ argRegs[1 + i] ~ "}";
						break;
					case accessMode.both:
				}
			} else {
				inps ~= "{" ~ argRegs[1 + i] ~ "}";
			}
		}
	}
}

private struct ExplicitMemory(accessMode mode, T) {
	T[] data;
}

/**** Provides different modes for memory access from syscalls.
	* 
	* These provide hints to the compiler as to how the given memory is affected.
	*/
enum accessMode {
	input,
	output,
	in_out,
}

/**** Provides hints to the syscall function about how the given memory will be accessed.
	* 
	* It accepts an object (may be an array) or a pointer and the access mode.
	* 
	* Returns: A secret type understood by syscall to specify explicit-access memory.
	*/
@nogc @safe auto explicitMem(accessMode mode, T)(T data) pure nothrow {
	static if (is(T U: U[]))
		return ExplicitMemory!(mode, U)(data);
	else static if (is(T U: U*))
		return ExplicitMemory!(mode, U)((cast(U[1])[*data])[]);
	else
		return ExplicitMemory!(mode, ubyte)((cast(ubyte[T.sizeof])cast(T[1])[data])[]);
}

@nogc @trusted ulong syscall(T...)(Syscall call, T args) nothrow {

	// Add a size_t at the start to signify the syscall number.
	alias stuff = argsToRegs!(0, size_t, T);

	static if (auto ptr = argRegs[$-1] in regShorts) {
		alias retOutput = AliasSeq!("[res] \"=" ~ *ptr ~ "\" res");
		alias retInstruct = AliasSeq!();
	} else {
		alias retOutput = AliasSeq!("[res] \"=rm\" res");
		alias retInstruct = AliasSeq!("mov %%" ~ argRegs[$-1] ~ ", %[res]");
	}

	enum inputs = staticJoin!(", ", stuff.inputs);
	enum outputs = staticJoin!(", ", AliasSeq!(stuff.outputs, retOutput));
	enum clobbers = "\"" ~ staticJoin!("\", \"", AliasSeq!(stuff.clobbers, clbRegs)) ~ "\"";
	enum instructs = staticJoin!("; ", AliasSeq!(stuff.instructs, asmExe, retInstruct));
	enum assembly = "asm { \"" ~ instructs ~ "\" : " ~ outputs ~ " : " ~ inputs ~ " : " ~ clobbers ~ ";}";

	// Actual running code
	size_t res;
	mixin(assembly);
	return res;
}
