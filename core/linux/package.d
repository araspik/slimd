/**** Linux-specific functionality.
  * 
  * This module contains various useful functions to use on Linux platforms, such as syscall.
	* 
	* Authors: ARaspiK
	* Date: May 24, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.linux;

public import core.linux.syscall,
							core.linux.time,
							core.linux.stdio;
