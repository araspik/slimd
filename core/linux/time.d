/**** Linux-specific time functions.
  * 
	* This module provides types and functions for structures relevant to Linux.
	* 
	* Authors: ARaspiK
	* Date: Jun 28, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.linux.time;

/**** Stores dates.
	*
	*/
struct timespec {
	size_t sec, nsec;
}
