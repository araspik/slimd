/**** Linux-specific standard I/O.
  * 
	* This module provides a Linux-specific internal standard input/output
	* implementation. Do not use it directly.
	* 
	* Authors: ARaspiK
	* Date: Jun 28, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.linux.stdio;

import core.linux;
import std.stdio;
import gcc.attribute;

/// Internal OS-dependent standard I/O implementation for Linux.
struct FileImpl {

	private uint fd = -1;

	public static shared immutable {
		auto stdin  = FileImpl(0);
		auto stdout = FileImpl(1);
		auto stderr = FileImpl(2);
	}

 @safe @nogc nothrow:

	/// Returns the OS-dependent file implementation from a file descriptor.
	this(uint fd) {
		this.fd = fd;
	}

 @attribute("forceinline"):

	/// Opens a file by file name.
	static FileImpl open(string name, FileMode mode, FilePerms perms) {
		import std.alloc: alloca;
		auto str = alloca!char(name.length + 1);
		str[0 .. $-1] = name[];
		str[$ - 1] = '\0';
		return FileImpl(cast(uint)syscall(Syscall.open,
					str.explicitMem!(accessMode.input), mode, perms));
	}

	/// Closes the file.
	void close() {
		syscall(Syscall.close, fd);
		fd = -1;
	}

	/// Writes the given data to the file.
	void put(const(ubyte)[] data) const {
		syscall(Syscall.write, fd, data.explicitMem!(accessMode.input), data.length);
	}

	/// Returns position in the file.
	/// TODO: Cache position.
	size_t pos() const {
		return syscall(Syscall.lseek, fd, 0, SeekType.cur);
	}

	/// Sets position in file.
	void pos(Offset off) {
		syscall(Syscall.lseek, fd, off.offset, off.type);
	}

	/// Syncs OS buffers for the file.
	void osSync() const {
		syscall(Syscall.fdatasync, fd);
	}

	/// Reads data into the given buffer.
	// TODO: std/stdio: Implement read buffer so that the user does not have to
	// reallocate. Additionally, the opApply for foreach loops would be cool.
	size_t get(ubyte[] data) const {
		return syscall(Syscall.read, fd, data.explicitMem!(accessMode.output),
				data.length);
	}

}
