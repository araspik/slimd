/**** Core atomic operation provider.
  * 
	* This module provides a nice interface for atomic operations.
	* 
	* Authors: ARaspiK
	* Date: Jul 22, 2018
	* License: MIT (see `license.txt` at project root)
  */

module core.atomics;

import gcc.builtins;
import gcc.attribute;

@trusted @nogc nothrow:

enum memOrder {
	raw,
	acq,
	rel,
	seq,
}

private template op2str(string op) {
	static if (op == "+")
		enum op2str = "add";
	else static if (op == "-")
		enum op2str = "sub";
	else static if (op == "&")
		enum op2str = "and";
	else static if (op == "|")
		enum op2str = "or";
	else static if (op == "!&")
		enum op2str = "nand";
	else
		enum op2str = "";
}

/**** Atomic operations.
	* 
	* Provides support for GCC builtin atomic operations.
	* TODO: More info
	*/
template atomicOp(string op, memOrder mem = memOrder.seq, T)
		if (isLockFree!T) {
	static if (op == "=") {
		@attribute("forceinline") T atomicOp(const ref shared T s) pure {
			// Load op
			static assert (mem != memOrder.raw);
			return __atomic_load_n(&s, mem);
		}
		@attribute("forceinline") void atomicOp(ref shared T d, const ref shared T s) pure {
			// Store op
			static assert (mem != memOrder.acq);
			__atomic_store(&s, &d, mem);
		}
	} else {
		// Op and return new
		@attribute("forceinline") T atomicOp(ref shared T d, const T v) pure {
			return mixin("__atomic_" ~ op2str!op ~ "_fetch(&d, v, mem)");
		}
	}
}

/**** Atomic exchange operations.
	* 
	* Provides support for GCC builtin atomic exchange-and-X operations.
	* By exchange-and-X performs old X val and returns old.
	*/
template atomicEx(string op, memOrder mem = memOrder.seq, T)
		if (isLockFree!T) {
	static if (op == "=") {
		// Exchange op
		@attribute("forceinline") T atomicEx(ref shared T d, const ref shared T s) pure {
			shared T res = void;
			__atomic_exchange(&d, &s, &res, mem);
			return res;
		}
	} else {
		// Op and return orig
		@attribute("forceinline") T atomicEx(ref shared T d, const T v) pure {
			return mixin("__atomic_fetch_" ~ op2str!op ~ "(&d, v, mem)");
		}
	}
}

@attribute("forceinline"):

/**** Atomic compare-and-exchange operation.
	* 
	* Provides support for GCC builtin compare-and-exchangeoperation.
	*/
bool atomicCmpEx(memOrder memGood = memOrder.seq, memOrder memFail = memOrder.seq, T)
			(ref shared T d, ref shared T e, const ref shared T b) pure
		if (isLockFree!T) {
	static assert (memFail != memOrder.raw && memFail <= memGood);
	return __atomic_compare_exchange(&d, &e, &b, true, memGood, memFail);
}

/**** Atomic test-and-set operation.
	* 
	*/
bool atomicTASet(memOrder mem = memOrder.seq)(ref shared bool val) pure {
	return __atomic_test_and_set(&val, mem);
}

/**** Atomic clear operation.
	* 
	*/
void atomicClear(memOrder mem = memOrder.seq)(ref shared bool val) pure {
	__atomic_clear(&val, mem);
}

/**** Atomic fence.
	*/
void atomicFence(memOrder mem)() {
	__atomic_thread_fence(mem);
}

/**** Returns whether the given type can be used with lock-free atomic operations.
	* 
	* Works at compile-time.
	* 
	* Assumes that objects for usage are aligned correctly.
	*/
bool isLockFree(T)() {
	return __atomic_always_lock_free(T.sizeof, 0);
}
