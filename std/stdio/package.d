/**** Standard I/O module.
  * 
	* This module provides file I/O operation functionality.
	* 
	* Authors: ARaspiK
	* Date: Jun 23, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.stdio;

version (linux) public import core.linux.stdio;

//import std.stdio.buffer;

/**** File lock types.
	* 
	* Provides an enumeration of possible types of locks a file (region) may have
	* applied.
	*/
enum LockType {
	none,
	ro,
	wo,
	rw,
}

/**** File buffer types.
	* 
	* Provides an enumeration of the types of file buffering provided.
	*/
enum BufferingType {
	none,
	line,
	full,
}

/**** File position presets.
	* 
	* Provides preset positions within a file for flexibility in file offset
	* specification.
	*/
enum SeekType {
	beg,
	cur,
	end,
}

/**** I/O mode.
	* 
	* Provides an enumeration of I/O modes supported.
	*/
enum IOMode {
	input,
	output,
	update,
}

/**** File permissions.
	*/
enum FilePerms {
	u_ro = 0b100 << 6,
	u_wo = 0b010 << 6,
	u_xo = 0b001 << 6,
	u_rw = 0b110 << 6,
	u_rx = 0b101 << 6,
	u_wx = 0b011 << 6,
	u_rwx = 0x111 << 6,

	g_ro = 0b100 << 3,
	g_wo = 0b010 << 3,
	g_xo = 0b001 << 3,
	g_rw = 0b110 << 3,
	g_rx = 0b101 << 3,
	g_wx = 0b011 << 3,
	g_rwx = 0x111 << 3,

	o_ro = 0b100,
	o_wo = 0b010,
	o_xo = 0b001,
	o_rw = 0b110,
	o_rx = 0b101,
	o_wx = 0b011,
	o_rwx = 0x111,
}

enum PermSet {
	ro = 0b100,
	wo = 0b010,
	xo = 0b001,
	rw = 0b110,
	wx = 0b011,
	rx = 0b101,
	rwx = 0b111,
}

@trusted @nogc FilePerms filePerms(PermSet user = PermSet.rw,
		PermSet group = PermSet.ro, PermSet other = PermSet.ro) nothrow pure {
	return cast(FilePerms)(user << 6 | group << 3 | other);
}

/**** File opening modes.
	* 
	* Provides an enumeration of modes to open files in.
	*/
enum FileMode {
	ro = 0,
	wo = 1,
	rw = 2,
	ap = 0x400 | wo,
	ua = ap | rw,

	create = 0x40,
	trunc  = 0x200,
}

/**** File type specification.
	* 
	* Provides an enumeration of valid file types.
	*/
enum FileType {
	file,
	directory,
	link,
}

/**** File offset specification.
	* 
	* Provides a means for specifying offsets within a file.
	* 
	* It contains a position preset (beginning, current, or ending) and an offset from
	* that preset. Together, they define an absolute offset within the file.
	*/
struct Offset {
 public: // Variables
	/// File position preset.
	SeekType type = void;
	/// Offset from preset in file.
	size_t offset = void;

 public: // Static variables

 public @safe @nogc nothrow: // Functions
	pure this(SeekType type, size_t offset) {
		this.type = type;
		this.offset = offset;
	}

	pure this(size_t offset) {
		this.type = SeekType.beg;
		this.offset = offset;
	}
}

/**** File storage.
	* 
	* Provides methods for performing (buffered) I/O on files and retrieving
	* information about files.
	*/
struct File {

	/// OS-dependent implementation.
	private FileImpl impl = void;

 public @safe @nogc nothrow:

	/// Opens the given file using the OS-dependent file implementation.
	private this(FileImpl impl) {
		this.impl = impl;
	}

	/// Opens the given file.
	void reopen(string name, FileMode mode) {
		this = File.open(name, mode);
	}

	@trusted static File open(string name, FileMode mode,
			FilePerms perms = FilePerms.u_rw | FilePerms.g_ro | FilePerms.o_ro) {
		return File(FileImpl.open(name, mode, perms));
	}

	/// Closes the file.
	void close() {
		impl.close();
		// No need to clear buffers. When opening another file, buffers would be cleared
		// anyways.
	}

	/// Writes data to the file.
	@trusted void put(const(ubyte)[] data) {
		/+if (bufW.type != BufferingType.none) // If buffered
			while (bufW.add(data)) // data is consumed
				impl.put(cast(const(ubyte)[])bufW.flush());
		else+/
			impl.put(data);
	}

	/// Flushes write buffers.
	@trusted void flush() {
		//if (bufW)
			//impl.put(cast(const(ubyte)[])bufW.flush());
	}

	/// Reads data from a file.
	ubyte[] get(ubyte[] data) const {
		return data[0 .. impl.get(data)];
	}

	/// Syncs OS buffers, if any.
	void sync() const {
		impl.osSync();
	}

	/// ditto
	T[] read(T)(T[] data) const {
		return cast(T[])get(cast(ubyte[])data);
	}

	/// Writes data to the file.
	@trusted void write(T...)(T args) {
		import std.ctfe;
		foreach (arg; args) {
			alias U = Unqual!(typeof(arg));
			static if (isSomeString!U) {
				put(cast(ubyte[])arg);
			} else {
				static assert (0, "Cannot print type " ~ U.stringof ~ " yet! TODO.");
			}
		}
	}

 @property:

	/// Returns the current position in the file.
	size_t position() {
		return impl.pos;
	}

	/// Sets the current position in the file.
	void position(size_t offset) {
		impl.pos(Offset(SeekType.beg, offset));
	}

	/// ditto
	void position(Offset offset) {
		impl.pos(offset);
	}

	/// Returns write buffer being used.
	T[] buffer(T = void)() {
		return null; //cast(T[])bufW.data;
	}

	/// Sets write buffer.
	void buffer(void[] data) {
		//bufW.data = data;
	}

	/// Sets write buffer type.
	void bufferType(BufferingType t) {
		//bufW.type = t;
	}

}

private static __gshared {
	auto _stdin  = File(FileImpl.stdin);
	auto _stdout = File(FileImpl.stdout);
	auto _stderr = File(FileImpl.stderr);
}

public @property @trusted @nogc nothrow {
	auto ref stdin()  {return _stdin;}
	auto ref stdout() {return _stdout;}
	auto ref stderr() {return _stderr;}
}
