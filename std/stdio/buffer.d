/**** File buffering capability.
  * 
	* This module provides file I/O buffering capability.
	* 
	* Note: Created on a Friday the 13th. Beware.
	* 
	* Authors: ARaspiK
	* Date: Jul 13, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.stdio.buffer;

import std.stdio;

/**** Provides file buffering cability.
	* 
	* The buffer does not call any I/O functions itself. The passed return values are
	* to be used by implementations of I/O to buffer.
	*/
struct FileBuffer {
	/// Underlying buffer.
	ubyte[] buf = void;
	/// Position in buffer.
	size_t pos = void;
	/// Buffer type: line-buffered (true) or full buffered (false).
	BufferingType buffering = BufferingType.none;

 public @safe @nogc nothrow: // Functions

	/**** Adds data to the buffer.
		* 
		* Adds data to the buffer.
		* If line-buffering is enabled, then it adds character-by-character, searching
		* for a newline. If it is found, then the data is immediately requested for
		* flushing.
		* Without line-buffering, it immediately fills the buffer with input data.
		* 
		* In both cases, if there was too much data, then the input data is sliced to
		* represent the data that was not added to the buffer and `true` is returned.
		* 
		* Params:
		*		data = Data to add to buffer.
		*					 If the buffer needs to be flushed, then it is modified (sliced) to
		*					 represent the data that was not added to the buffer.
		* 
		* Returns: `true` if the buffer needs to be flushed, else `false`.
		*/
	bool add(ref const(ubyte)[] data) {
		assert (buffering != BufferingType.none);
		if (buffering == BufferingType.line) {
			// Line-buffered: Loop each character to find a newline.
			size_t i = 0;
			while ((buf[pos++] = data[i]) != '\n') i++;
			if (i < data.length) {
				data = data[i .. $];
				return true;
			}
		} else {
			size_t avail = buf.length - pos;
			if (avail < data.length) {
				buf[pos .. $] = data[0 .. avail];
				pos += avail;
				data = data[avail .. $];
				return true;
			} else {
				buf[pos .. pos + data.length] = data[];
				pos += data.length;
			}
		}
		return false;
	}

	/// Flush buffer, returning old contents up to current position.
	T[] flush(T = void)() {
		assert (buf !is null);
		size_t tmp = pos;
		pos = 0;
		return cast(T[])buf[0 .. tmp];
	}

	/// "Clear" buffer by resetting pos
	void clear() pure {
		pos = 0;
	}

	T opCast(T : bool)() pure const {
		return buffering != BufferingType.none;
	}

 @property pure: // Property functions

	/// Returns underlying buffer.
	T[] data(T = void)() const {
		return cast(T[])buf;
	}

	/// Sets underlying buffer.
	@trusted void data(void[] buf) {
		clear();
		this.buf = cast(ubyte[])buf;
	}

	/// Sets buffer type.
	void type(BufferingType t) {
		buffering = t;
	}

	/// Gets buffer type.
	BufferingType type() {
		return buffering;
	}
}
