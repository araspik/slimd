/**** A collection of random number generators.
  * 
	* This module consists of various random number generators, most of which will NOT be
	* cryptographically secure. Ones which are are marked as such.
	* 
	* Authors: ARaspiK
	* Date: May 27, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.rng;

version(linux) public import std.rng.urandom;
