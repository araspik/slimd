/**** An interface to Linux's builtin urandom PRNG.
  * 
	* This module provides access to Linux's urandom source, which can be used to both recieve a
	* random sequence of bytes and create random numbers (which is just the former fixed at
	* integer length).
	* 
	* Authors: ARaspiK
	* Date: May 27, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.rng.urandom;

public import core.linux.urandom;
