/**** SlimD CTFE functionality.
  * 
	* This module provides compile-time functionality for various objects like typelists,
	* templates, etc. that all work at compile-time.
	* It also provides methods for differentiating types.
	* 
	* Authors: ARaspiK
	* Date: May 23, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.ctfe;

public import std.ctfe.types,
							std.ctfe.meta;
