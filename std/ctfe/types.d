/**** Compile-time type checking.
  * 
	* This module provides templates for distinguishing various types by various attributes.
	* 
	* Authors: ARaspiK
	* Date: May 23, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.ctfe.types;

/**** Returns whether the given type is an aggregate.
	*/
enum bool isAggregateType(T) = is(T == struct) || is(T == union)
		|| is(T == class) || is(T == interface);

/**** Returns whether the given type is an array.
	*/
enum bool isArray(T) = is(T: U[], U);

/**** Returns whether the given type is a static array.
	*/
enum bool isStaticArray(T) = __traits(isStaticArray, T);

/**** Returns whether the given type is a dynamic array.
	*/
enum bool isDynamicArray(T) = is(T: U[], U) && !isStaticArray!T;

/**** Returns whether the given type is an associative array.
	*/
enum bool isAssociativeArray(T) = __traits(isAssociativeArray, T);

/**** Returns whether the type is an integral type.
	*/
enum bool isIntegral(T) = __traits(isIntegral, T);

/**** Returns whether the type is an unsigned type.
	*/
enum bool isUnsigned(T) = isNumeric!T && __traits(isUnsigned, T);

/**** Returns whether the type is a signed type.
	*/
enum bool isSigned(T) = isNumeric!T && !__traits(isUnsigned, T);

/**** Returns whether the type is a floating-point type.
	*/
enum bool isFloating(T) = __traits(isFloating, T);

/**** Checks whether the type is a builtin in numeric type (integral or floating-point).
	*/
enum bool isNumeric(T) = __traits(isArithmetic, T) && isNoneOf!(Unqual!T, bool, char, wchar, dchar);

/**** Checks that the first type is not any of the other types.
	*/
template isNoneOf(E, T...) {
	static if (T.length == 0) enum isNoneOf = true;
	else static if (T.length == 1) enum isNoneOf = !is(E == T[0]);
	else enum isNoneOf = isNoneOf!(E, T[0..$/2]) && isNoneOf!(E, T[$/2..$]);
}

/**** Checks that the first type is exactly one of the other types.
	*/
template isOneOf(E, T...) {
	static if (T.length == 0) enum isOneOf = false;
	else static if (T.length == 1) enum isOneOf = is(E == T[0]);
	else {
		enum inA = isOneOf!(E, T[0..$/2]);
		enum inB = isOneOf!(E, T[$/2..$]);
		enum isOneOf = (inA && !inB) || (!inA && inB); // One or the other, neither both nor none
	}
}

/**** Checks if the given type is a string.
	*/
template isSomeString(T) {
	static if (is(Unqual!T == string) || is(Unqual!T == wstring) ||
			is(Unqual!T == dstring))
		enum isSomeString = true;
	else {
		static if (is(Unqual!T U: U[])) {
			static if (is(U == char) || is(U == wchar) || is(U == dchar)) {
				enum isSomeString = true;
			} else
				enum isSomeString = false;
		} else
			enum isSomeString = false;
	}
}

/**** Provides the unqualified version of a given type.
	*/
template Unqual(T) {
	static if (
				 is(T U == const U)
			|| is(T U == immutable U)
			|| is(T U == inout U)
			|| is(T U == shared U)
		) alias Unqual = Unqual!U;
	else alias Unqual = T;
}

/**** Provides a type that all given types can be converted to.
	*/
template CommonType(T...) {
	static if (!T.length)
		alias CommonType = void;
	else static if (T.length == 1) {
		static if (is(typeof(T[0])))
			alias CommonType = typeof(T[0]);
		else
			alias CommonType = T[0];
	} else static if (is(typeof(true ? T[0].init : T[1].init) U))
		alias CommonType = CommonType!(U, T[2..$]);
	else
		alias CommonType = void;
}

/**** Provides the least value of a given type.
	* 
	* It provides uniformity across both floating types (-T.max) and integers (T.min).
	*/
template leastValueOf(T) {
	
}

template FunctionTypeOf(func...)
    if (func.length == 1 && isCallable!func)
{
    static if (is(typeof(& func[0]) Fsym : Fsym*) && is(Fsym == function) || is(typeof(& func[0]) Fsym == delegate))
    {
        alias FunctionTypeOf = Fsym; // HIT: (nested) function symbol
    }
    else static if (is(typeof(& func[0].opCall) Fobj == delegate))
    {
        alias FunctionTypeOf = Fobj; // HIT: callable object
    }
    else static if (is(typeof(& func[0].opCall) Ftyp : Ftyp*) && is(Ftyp == function))
    {
        alias FunctionTypeOf = Ftyp; // HIT: callable type
    }
    else static if (is(func[0] T) || is(typeof(func[0]) T))
    {
        static if (is(T == function))
            alias FunctionTypeOf = T;    // HIT: function
        else static if (is(T Fptr : Fptr*) && is(Fptr == function))
            alias FunctionTypeOf = Fptr; // HIT: function pointer
        else static if (is(T Fdlg == delegate))
            alias FunctionTypeOf = Fdlg; // HIT: delegate
        else
            static assert(0);
    }
    else
        static assert(0);
}

template ReturnType(func...)
    if (func.length == 1 && isCallable!func)
{
    static if (is(FunctionTypeOf!func R == return))
        alias ReturnType = R;
    else
        static assert(0, "argument has no return type");
}

template Parameters(func...)
    if (func.length == 1 && isCallable!func)
{
    static if (is(FunctionTypeOf!func P == function))
        alias Parameters = P;
    else
        static assert(0, "argument has no parameters");
}

template isCallable(T...)
    if (T.length == 1)
{
    static if (is(typeof(& T[0].opCall) == delegate))
        // T is a object which has a member function opCall().
        enum bool isCallable = true;
    else static if (is(typeof(& T[0].opCall) V : V*) && is(V == function))
        // T is a type which has a static member function opCall().
        enum bool isCallable = true;
    else
        enum bool isCallable = isSomeFunction!T;
}


template isSomeFunction(T...)
    if (T.length == 1)
{
    static if (is(typeof(& T[0]) U : U*) && is(U == function) || is(typeof(& T[0]) U == delegate))
    {
        // T is a (nested) function symbol.
        enum bool isSomeFunction = true;
    }
    else static if (is(T[0] W) || is(typeof(T[0]) W))
    {
        // T is an expression or a type.  Take the type of it and examine.
        static if (is(W F : F*) && is(F == function))
            enum bool isSomeFunction = true; // function pointer
        else
            enum bool isSomeFunction = is(W == function) || is(W == delegate);
    }
    else
        enum bool isSomeFunction = false;
}
