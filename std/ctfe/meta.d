/**** SlimD CTFE metaprogramming and template manipulation routines.
  * 
	* This module provides compile-time functionality for various objects like typelists,
	* templates, etc. that all work at compile-time.
	* 
	* Authors: ARaspiK
	* Date: May 23, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.ctfe.meta;

import std.ctfe.types;

/**** Creates a sequence (compile-time "tuple") of aliases.
	*/
alias AliasSeq(T...) = T;

/**** Allows aliasing any symbol.
	*/
alias Alias(alias a) = a;

/// ditto
alias Alias(T) = T;

/**** Stores the symbol as either an alias (preferred) or an enum.
	* 
	* If storing as any is not possible, then an assert error is thrown.
	*/
template SymbolStore(alias a) {
	static if (__traits(compiles, {alias x = a;}))
		alias SymbolStore = a;
	else static if (__traits(compiles, {enum x = a;}))
		enum SymbolStore = a;
	else
		static assert(0, "Cannot store " ~ a.stringof ~ " as either an alias or enum!");
}

/// ditto
template SymbolStore(T)
		if (!isAggregateType!T || is (Unqual!T == T)) {
	alias SymbolStore = T;
}

/**** Can be used to check if a given argument is a type.
	*
	* If it is not, compilation fails. Use it within a __traits(compiles, ...).
	*/
private template compileType(T) {}

/**** Can be used to check if a given argument is a bool.
	* 
	* If it is not, compilation fails. Use it within a __traits(compiles, ...).
	*/
private template compileBool(bool b) {}

/**** Can be used to check if two symbols are the same.
	* 
	* It works for types, literals, variables, functions, templates, etc.
	* 
	* Implementation:
		* Both are types: `is(a == b)`
		* Both are literals (or enums): `a == b`
		* Anything else: `__traits(isSame, a, b)`
	*/
template isSame(ab...) if (ab.length == 2) {
	static if (__traits(compiles, compileType!(ab[0]), compileType!(ab[1])))
		enum isSame = is(ab[0] == ab[1]);
	else static if (!__traits(compiles, compileType!(ab[0]), compileType!(ab[1])) &&
				__traits(compiles, compileBool!(ab[0] == ab[1])) &&
				!__traits(compiles, &ab[0], &ab[1]))
		enum isSame = (ab[0] == ab[1]);
	else
		enum isSame = __traits(isSame, ab[0], ab[1]);
}

/**** Returns the index of the first occurence of a symbol within a group of symbols.
	*/
template staticIndexOf(T...) if (T.length >= 1) {
	static if (T.length == 1)
		enum staticIndexOf = -1;
	else static if (T.length == 2) {
		alias a = SymbolStore!(T[0]);
		alias b = SymbolStore!(T[1]);
		enum staticIndexOf = (isSame!(a, b)) ? 0 : -1;
	} else {
		alias t = T[1..$];
		enum a = staticIndexOf!(T[0], t[0..$/2]);
		enum b = staticIndexOf!(T[0], t[$/2..$]);
		static if (a != -1)
			enum staticIndexOf = a;
		else static if (b != -1)
			enum staticIndexOf = b + t.length / 2;
		else
			enum staticIndexOf = -1;
	}
}

/**** Returns an AliasSeq of the given type list, with all instances of the first
	* argument removed.
	*/
template staticRemoveAll(T...) if (T.length >= 1) {
	static if (T.length == 1)
		alias staticRemoveAll = AliasSeq!();
	else static if (T.length == 2) {
		alias a = SymbolStore!(T[0]);
		alias b = SymbolStore!(T[1]);
		static if (isSame!(a, b))
			alias staticRemoveAll = AliasSeq!(b);
		else
			alias staticRemoveAll = AliasSeq!();
	} else {
		alias t = T[1..$];
		alias a = staticRemoveAll!(T[0], t[0..$/2]);
		alias b = staticRemoveAll!(T[0], t[$/2..$]);
		alias staticRemoveAll = AliasSeq!(a, b);
	}
}

/**** Returns an AliasSeq of the given type list, with all instances of the first
	* argument replaced by the second argument.
	*/
template staticReplace(T...) if (T.length >= 2) {
	static if (T.length == 2)
		alias staticRemoveAll = AliasSeq!();
	else static if (T.length == 3) {
		alias a = SymbolStore!(T[0]);
		alias b = SymbolStore!(T[2]);
		alias c = SymbolStore!(T[1]);
		alias staticReplace = AliasSeq!((isSame!(a, b)) ? c : a);
	} else {
		alias t = T[2..$];
		alias a = staticReplace!(T[0], T[1], t[0..$/2]);
		alias b = staticReplace!(T[0], T[1], t[$/2..$]);
		alias staticReplace = AliasSeq!(a, b);
	}
}

/**** Performs a higher-level map function on the given type list, with the function
	* being the first argument.
	*/
template staticMap(alias F, T...) {
	static if (T.length == 0)
		alias staticMap = AliasSeq!();
	else static if (T.length == 1)
		alias staticMap = AliasSeq!(F!(T[0]));
	else {
		alias a = staticMap!(F, T[0..$/2]);
		alias b = staticMap!(F, T[$/2..$]);
		alias staticMap = AliasSeq!(a, b);
	}
}

/**** Performs a higher-level filter function on the given type list, with the
	* function being the first argument.
	*/
template staticFilter(alias F, T...) {
	static if (T.length == 0)
		alias staticFilter = AliasSeq!();
	else static if (T.length == 1) {
		static if (F!(T[0]))
			alias staticFilter = AliasSeq!(T[0]);
		else
			alias staticFilter = AliasSeq!();
	} else {
		alias a = staticFilter!(F, T[0..$/2]);
		alias b = staticFilter!(F, T[$/2..$]);
		alias staticFilter = AliasSeq!(a, b);
	}
}

/**** Joins an array of objects together at compile time.
	* 
	* The objects must be of the same type, and they must be joinable using the
	* concatenation (`~`) operator.
	*/
template staticJoin(S...)
		if ( S.length >= 1) {
	static if (S.length == 1)
		enum staticJoin = typeof(S[0]).init;
	else static if (S.length == 2)
		enum staticJoin = S[1];
	else {
		enum t = S[1..$];
		enum half0 = staticJoin!(S[0], t[0 .. $/2]);
		enum half1 = staticJoin!(S[0], t[$/2 .. $]);
		enum staticJoin = half0 ~ S[0] ~ half1;
	}
}

/**** Converts a range or array to an AliasSeq.
	*/
template aliasSeqOf(alias r) if (isArray!(typeof(r))) {
	static if (r.length == 0)
		alias aliasSeqOf = AliasSeq!();
	else static if (r.length == 1)
		alias aliasSeqOf = AliasSeq!(r[0]);
	else {
		alias a = aliasSeqOf!(r[0..$/2]);
		alias b = aliasSeqOf!(r[$/2..$]);
		alias aliasSeqOf = AliasSeq!(a, b);
	}
}
