/**** A module of various memory allocators.
  * 
  * This package module selects and imports a suitable allocator module for the system.
	* 
	* Authors: ARaspiK
	* Date: May 25, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.alloc;

public import std.alloc.alloca;
