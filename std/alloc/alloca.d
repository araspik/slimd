/** Intrinsic builtin stack-space allocator.
  * 
	* This module provides variants of alloca, as provided by GCC.
	* It is allocating variable space on the stack.
	* This module removes the need of __builtin_* function calls as well as provides overloads and
	* templates to ease allocating space for individual variables and arrays.
	* 
	* Authors: ARaspiK
	* Date: Jul 9, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.alloc.alloca;

import gcc.attribute;
import gcc.builtins;

@trusted @nogc public @attribute("forceinline") nothrow:

/**** Allocates space on the stack for an object of the given type.
	* 
	* It utilizes GCC's builtin `alloca` function and allocates space for the given type, returning the
	* allocated type.
	*/
ref T alloca(T)()
		if (__traits(isPOD, T)) {
	return *cast(T*)__builtin_alloca(T.sizeof);
}

/**** Allocates space on the stack for an array of the given type and length.
	* 
	* It utilizes GCC's builtin `alloca` function and allocates space for an array of objects for the
	* given type.
	*/
T[] alloca(T = void)(size_t num)
		if (__traits(isPOD, T)) {
	return (cast(T*)__builtin_alloca(T.sizeof * num))[0 .. num];
}
