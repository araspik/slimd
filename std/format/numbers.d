/**** Number formatting functionality.
  * 
	* This module provides functionality to convert numbers to strings, for both unsigned and signed
	* numbers of any base.
	* 
	* Authors: ARaspiK
	* Date: Jul 4, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.format.numbers;

/+
@nogc @safe nothrow pure:

/**** Formats a mini-block of numbers.
	*/
private template itoa(size_t base, T)
		if (isIntegral!T) {

	alias dblT = uintX!(T.sizeof * 8 * 2);

	private ulong getdigs(size_t b, size_t n) {
		return cast(ulong)(n / log2(cast(real)b));
	}
	
	// Figure out maximum number of digits at compile time.
	enum mdig = getdigs(base, T.sizeof * 8); // log(2^n)/log(base) = log2(2^n)/log2(base) = n / log2(base)
	pragma(msg, mdig);

	// Following Agner Fog's asmlib:divfixedi64.asm

	// divisor
	enum d = base;
	// integer size, bits
	enum n = T.sizeof * 8;
	// ceil(log2(d))
	enum L = (d - 1).bsr() + 1;
	// 1 + 2^n * (2^L - d) / d
	enum m = 1 + cast(T)((dblT((1 << L) - d) << n) / d);
	// min(L, 1)
	enum sh1 = cast(T)(L >= 1);
	// max(L-1, 0)
	enum sh2 = (-cast(T)(L > 1)) & L;

	pragma(msg, mdig);

	// Actual work: Divide
	T itoa(T a) {
		dblT t = m;
		t *= a;
		a -= cast(T)t;
		a >>= sh1;
		a += cast(T)t;
		a >>= sh2;
		return a;
	}

}

pragma(msg, itoa!10(20));
+/
