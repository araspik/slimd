/**** A big integer type.
  * 
	* This module provides a 2^n big integer implementation.
	* 
	* Authors: ARaspiK
	* Date: Jun 5, 2018
	* License: MIT (see `license.txt` at project root) (d-game-dev: unlicense)
  */

module std.math.bigint;

import std.ctfe;

alias  intX(size_t bits) = xintX!(true, bits);
alias uintX(size_t bits) = xintX!(false, bits);

alias  int128 =  intX!128;
alias uint128 = uintX!128;
alias  int256 =  intX!256;
alias uint256 = uintX!256;

template xintX(bool signed, size_t bits)
		if ((bits & (bits - 1)) == 0 && bits <= 64) {

	static if (!signed) {
		static if (bits == ubyte.sizeof * 8)
			alias xintX = ubyte;
		else static if (bits == ushort.sizeof * 8)
			alias xintX = ushort;
		else static if (bits == uint.sizeof * 8)
			alias xintX = uint;
		else static if (bits == ulong.sizeof * 8)
			alias xintX = ulong;
	} else {
		static if (bits == byte.sizeof * 8)
			alias xintX = byte;
		else static if (bits == short.sizeof * 8)
			alias xintX = short;
		else static if (bits == int.sizeof * 8)
			alias xintX = int;
		else static if (bits == long.sizeof * 8)
			alias xintX = long;
	}
}

struct xintX(bool signed, size_t bits)
		if ((bits & (bits - 1)) == 0 && bits >= 128) {

	private {

		alias self = xintX!(signed, bits);

		enum bool isSelf(T) = is(T == self);

		alias  intb2 =  intX!(bits / 2);
		alias uintb2 = uintX!(bits / 2);

		alias  intb4 =  intX!(bits / 4);
		alias uintb4 = uintX!(bits / 4);

		alias intHi = xintX!(signed, bits / 2);
		alias intLo = uintb2;

		version (LittleEndian) {
			intLo lo;
			intHi hi;
		} else {
			intHi hi;
			intLo lo;
		}

	}

 @nogc nothrow pure:

	/// Constructor: Works like assignment
	this(T)(T n) {
		opAssign(n);
	}

	/// Assignment: Unsigned integer
	ref self opAssign(T)(const T n)
			if (isUnsigned!T) {
		hi = 0;
		lo = n;
		return this;
	}

	/// Assignment: Signed integer (sign-extended)
	ref self opAssign(T)(const T n)
			if (isSigned!T) {
		hi = cast(intHi)(n < 0 ? -1 : 0);
		lo = cast(intb2)n;
		return this;
	}

	/// Assignment: Same-size wide enteger (sign lost)
	ref self opAssign(T: xintX!(bSigned, bits), bool bSigned)(const T n) {
		hi = n.hi;
		lo = n.lo;
		return this;
	}

	/// Assignment: Smaller wide integer (sign-extended)
	ref self opAssign(T: xintX!(bSigned, bBits), bool bSigned, size_t bBits)(const T n)
			if (bBits < bits) {
		static if (bSigned) {
			hi = cast(intHi)(n < 0 ? -1 : 0);
			lo = cast(intb2)n;
		} else {
			hi = 0;
			lo = n;
		}
		return this;
	}

	/// Assginment: larger wide integer (truncation)
	ref self opAssign(T: xintX!(bSigned, bBits), bool bSigned, size_t bBits)(const T n)
			if (bBits > bits) {
		enum shift = bits / 2;
		hi = cast(intb2)(n >> shift);
		lo = cast(intb2)n;
		return this;
	}

	/// Cast: Smaller integer (truncation)
	T opCast(T)() const
			if (isIntegral!T) {
		return cast(T)lo;
	}

	/// Cast: Boolean
	T opCast(T : bool)() const {
		return this != 0;
	}

	/// Cast: Wide integer
	T opCast(T : xintX!(bSigned, bBits), bool bSigned, size_t bBits)() const {
		static if (bBits < bits)
			return cast(T)lo;
		else
			return T(this);
	}

	alias opBinaryRight = opBinary;

	/// Binary operations
	self opBinary(string op, T)(const T b) const {
		self r = this;
		return r.opOpAssign!op(b);
	}

	/// Assignment-operators: Other types
	ref self opOpAssign(string op, T)(const T b)
			if (!isSelf!T) {
		const self o = b;
		return opOpAssign!op(o);
	}

	/// Assignment-operators: This type
	ref self opOpAssign(string op)(const self b) {
		static if (op == "+") {
			hi += b.hi;
			if (lo + b.lo < lo) ++hi;
			lo += b.lo;
		} else static if (op == "-") this += -b;
		else static if (op == "<<") {
			if (b >= bits) {
				hi = 0;
				lo = 0;
			} else if (b >= bits / 2) {
				hi = lo << (b.lo - bits / 2);
				lo = 0;
			} else if (b > 0) {
				hi = (lo >>> (bits / 2 - b.lo)) | (hi << b.lo);
				lo <<= b.lo;
			}
		} else static if (op == "&" || op == "|" || op == "^") {
			mixin("hi " ~ op ~ "= b.hi;");
			mixin("lo " ~ op ~ "= b.lo;");
		} else static if (op == ">>" || op == ">>>") {
			immutable auto signFill = cast(intb2)((!signed || op == ">>>") ? 0 : (isNegative() ? -1 : 0));

			if (b >= bits) {
				hi = signFill;
				lo = signFill;
			} else if (b >= bits / 2) {
				lo = hi >> (b.lo - bits / 2);
				hi = signFill;
			} else if (b > 0) {
				lo = (hi << (bits / 2 - b.lo)) | (lo >> b.lo);
				hi >>= b.lo;
			}
		} else static if (op == "*") {
			auto a = parts, o = b.parts;
			this = 0;

			foreach (i; 0..4)
				foreach (j; 0..4)
					this += self(cast(intb2)a[i] * o[j]) << ((bits / 4) * (i + j));
		} else static if (op == "/" || op == "%") {
			assert (b != 0);
			self q = 0, div = this, d = b;

			static if (signed) {
				div = div.abs;
				d = d.abs;
			}

			while (d <= div) {
				self N = 0, cd = d;

				while (div > cd) {
					if (cd.signBit || div < (cd << 1)) break;
					cd <<= 1;
					++N;
				}

				div -= cd;
				q += self(1) << N;
			}

			static if (signed) {
				if (this < 0) div = -div;
				if ((this >= 0) != (b >= 0))
					q = -q;
			}

			static if (op == "/")
				this = q;
			else
				this = div;
		} else {
			static assert (false, "Unsupported operation on xintX: '" ~ op ~ "'");
		}

		return this;
	}

	/// Unary operations: +a, -a, ~a. Constant operations.
	self opUnary(string op)() const
			if (op == "-" || op == "+" || op == "~") {
		static if (op == "-") {
			self r = ~this;
			return ++r;
		} else static if (op == "+") {
			return this;
		} else static if (op == "~") {
			self r = void;
			r.hi = ~hi;
			r.lo = ~lo;
			return r;
		}
	}

	/// Unary operations: Non-constant in/de-crements.
	ref self opUnary(string op)()
			if (op == "++" || op == "--") {
		static if (op == "++") {
			++lo;
			if (lo == 0) ++hi;
		} else static if (op == "--") {
			if (lo == 0) --hi;
			--lo;
		}
		return this;
	}

	/// Comparison: Equals: Other types
	bool opEquals(T)(const T b) const
			if (!isSelf!T) {
		return this == self(b);
	}

	/// Comparison: Equals: This type
	bool opEquals(const self b) const {
		return lo == b.lo && hi == b.hi;
	}

	/// Comparison: Other types
	int opCmp(T)(const T b) const
			if (!isSelf!T) {
		return opCmp(self(b));
	}

	/// Comparison: This type
	int opCmp(const self b) const {
		if (hi < b.hi) return -1;
		if (hi > b.hi) return 1;
		if (lo < b.lo) return -1;
		if (lo > b.lo) return 1;
		return 0;
	}

 private: // Utils

	bool isNegative() const {
		static if (signed)
			return signBit();
		else
			return false;
	}

	bool signBit() const {
		enum bitLoc = bits / 2 - 1;
		return ((hi >> bitLoc) & 1) != 0;
	}

	@property intb4[4] parts() const {
		enum shift = bits / 4;
		immutable mask = cast(intb2)cast(intb4)-1;

		intb4[4] res = void;
		res[0] = cast(intb4)(lo & mask);
		res[1] = cast(intb4)(lo >> shift);
		res[2] = cast(intb4)(hi & mask);
		res[3] = cast(intb4)(hi >> shift);

		return res;
	}

}

@safe @nogc T abs(T : xintX!(signed, bits), bool signed, size_t bits)(const T o) pure nothrow {
	return (o >= 0) ? o : -o;
}
