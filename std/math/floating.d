/**** Various utilities and functions for floating-point types.
  * 
	* This module provides floating point utilities, information (binary formats), and functions for
	* floating-point types.
	* 
	* Authors: ARaspiK
	* Date: Jul 6, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.math.floating;

import gcc.builtins;
import std.ctfe;
import core.math.floatinfo;

@nogc @safe pure nothrow:

bool isNaN(T)(const T val)
		if (isFloating!T) {
	enum F = floatInfo!T;

	return F.getExponent(val) == F.expMask && F.mantissaNonZero(val);
}

@trusted bool isInfinity(T)(const T val)
		if (isFloating!T) {
	enum F = floatInfo!T;

	switch (F.format) {
		case FloatFormat.ieeeSingle:
		case FloatFormat.ieeeDouble:
		case FloatFormat.ieeeQuadruple:
			return F.getExponent(val) == F.expMask && !(F.mantissaNonZero(val));
		case FloatFormat.ieeeExtended:
			return F.getExponent(val) == F.expMask && ((*cast(ulong*)&val) & 0x0000_7ffff_ffff_ffff) == 0;
		case FloatFormat.ibmExtended:
			return ((cast(ulong*)&val)[F.mantissaMSB] & 0x7fff_ffff_ffff_ffff)
					== 0x7ff8_0000_0000_0000;
		default:
			return (val < -T.max) || (val > T.max);
	}
}

/**** Returns |x|.
	* 
	*/
T abs(T)(const T val)
		if (isFloating!T) {
	enum F = floatInfo!T;

	switch (F.format) {
		case FloatFormat.ieeeSingle:
			return __builtin_fabsf(val);
		case FloatFormat.ieeeDouble:
			return __builtin_fabs(val);
		default:
			return __builtin_fabsl(val);
	}
}
