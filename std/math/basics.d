/**** Common math functions.
  * 
	* This module contains replacements for C's math functions in `stdlib.h`.
	* 
	* Authors: ARaspiK
	* Date: Jun 5, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.math.basics;

import std.ctfe;

@nogc nothrow @safe pure:

enum real E =          0x1.5bf0a8b1457695355fb8ac404e7a8p+1L; /** e = 2.718281... */
enum real LOG2T =      0x1.a934f0979a3715fc9257edfe9b5fbp+1L; /** log2(10) = 3.321928... */
enum real LOG2E =      0x1.71547652b82fe1777d0ffda0d23a8p+0L; /** log2(e) = 1.442695... */
enum real LOG2 =       0x1.34413509f79fef311f12b35816f92p-2L; /** log10(2) = 0.301029... */
enum real LOG10E =     0x1.bcb7b1526e50e32a6ab7555f5a67cp-2L; /** log10(e) = 0.434294... */
enum real LN2 =        0x1.62e42fefa39ef35793c7673007e5fp-1L; /** ln(2)  = 0.693147... */
enum real LN10 =       0x1.26bb1bbb5551582dd4adac5705a61p+1L; /** ln(10) = 2.302585... */
enum real PI =         0x1.921fb54442d18469898cc51701b84p+1L; /** pi = 3.141592... */
enum real PI_2 =       PI/2;                                  /** pi / 2 = 1.570796... */
enum real PI_4 =       PI/4;                                  /** pi / 4 = 0.785398... */
enum real M_1_PI =     0x1.45f306dc9c882a53f84eafa3ea69cp-2L; /** 1 / pi = 0.318309... */
enum real M_2_PI =     2*M_1_PI;                              /** 2 / pi = 0.636619... */
enum real M_2_SQRTPI = 0x1.20dd750429b6d11ae3a914fed7fd8p+0L; /** 2 / sqrt(pi) = 1.128379... */
enum real SQRT2 =      0x1.6a09e667f3bcc908b2fb1366ea958p+0L; /** sqrt(2) = 1.414213... */
enum real SQRT1_2 =    SQRT2/2;                               /** sqrt(1/2) = 0.707106... */

bool sign(T)(const T val)
		if (isSigned!T && !isFloating!T) {
	return val >>> (T.sizeof * 8 - 1); // Unsigned shift
}

T abs(T)(const T val)
		if (isSigned!T && !isFloating!T) {

	T sign = val >> (T.sizeof * 8 - 1); // Sign filled
	return (sign ^ val) - sign;
}

struct divres(T)
		if (isIntegral!T) {
	version (LittleEndian)
		T quo, rem;
	else version(BigEndian)
		T rem, quo;
}

divres!T div(T)(const T dividend, const T divisor)
		if (isIntegral!T) {
	return divres!T(dividend / divisor, dividend % divisor);
}

bool isPowerOfTwo(T)(T val)
		if (isIntegral!T) {
	return (val & (val - 1)) == 0;
}
