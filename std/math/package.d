/**** Provides standard math functionality.
  * 
	* This package provides common math functions.
	* 
	* Authors: ARaspiK
	* Date: Jul 9, 2018
	* License: MIT (see `license.txt` at project root)
  */

module std.math;

public import std.math.basics,
							std.math.bigint,
							std.math.floating;
